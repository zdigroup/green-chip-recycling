<?php
/**
 * Created by PhpStorm.
 * User: derekmiranda
 * Date: 7/23/14
 * Time: 6:59 PM
 */
return array(

    /**
     * Mail Config
     *
     * Override Email Recipients for local environment only
     */
    'mail'=>array(
        'override'=>array(
            'enabled'=>true,
            'address'=>'bryan@zdidesign.com',
        ),
    ),
);
