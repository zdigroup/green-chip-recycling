<?php

/**
 *
 *
 * *************************************************
 *
 * STOP:
 * This file should only include files that are truly global to the application.
 * Remember, we may have other modules, like an Admin, and we shouldn't be making
 * assumptions about what that module requires. So, this file should only contain
 * relatively benign things, like bootstrap or jquery. Plus, it could end up overriding
 * things.
 *
 * Other wise, you should follow the MVC pattern of the MVC Asset Manager as described
 * in the docs (https://bitbucket.org/spectravp/mvc-asset-loader-2) and put your declarations in
 * module/Application/config/assets.local.php
 *
 * *************************************************
 *
 *
 */


$commonCssLibraries = array(
    '/vendor/font-awesome/css/all.min.css'=>array('load'=>true, 'check_file'=>false,),
    '/vendor/font-awesome/css/v4-shims.min.css'=>array('load'=>true, 'check_file'=>false,),
    '/vendor/slick-carousel/slick/slick.css'=>array('load'=>true, 'check_file'=>false,),
    '/vendor/magnific-popup/dist/magnific-popup.css' => array ('load' => true, 'check_file' => false),
    '/vendor/ng-dialog/css/ngDialog.css'=>array('load'=>true, 'check_file'=>false,),
    '/vendor/ng-dialog/css/ngDialog-theme-default.css'=>array('load'=>true, 'check_file'=>false,),
    '/vendor/angular-custom-dropdown-v2/css/angular-custom-dropdown.min.css'=>array('load'=>true, 'check_file'=>false),
    '/vendor/sweetalert/dist/sweetalert.css'=>array('load'=>true, 'check_file'=>false),
);

$commonJsLibraries = array('/vendor/jquery/dist/jquery.min.js'=>array('load'=>true, 'check_file'=>false,),
    'https://maps.googleapis.com/maps/api/js?key=AIzaSyArvxDDq2DKZzsnYqaWvQ3ngDnfarN6r70&libraries=places'=>array('load'=>true, 'check_file'=>false),
    '/vendor/angular/angular.min.js'=>array('load'=>true, 'check_file'=>false,),
    '/vendor/format-as-currency/dist/format-as-currency.js'=>array('load'=>true, 'check_file'=>false,),
    '/vendor/api-validation-messages/dist/api-validation-messages.js'=>array('load'=>true, 'check_file'=>false,),
    '/vendor/modernizr/modernizr.min.js'=>array('load'=>true, 'check_file'=>false,),
    '/vendor/skrollr/dist/skrollr.min.js'=>array('load'=>true, 'check_file'=>false,),
    '/vendor/angular-click-outside/clickoutside.directive.js'=>array('load'=>true, 'check_file'=>false,),
    '/vendor/ngMask/dist/ngMask.min.js'=>array('load'=>true, 'check_file'=>false,),
    '/vendor/ngstorage/ngStorage.min.js'=>array('load'=>true, 'check_file'=>false,),
    '/vendor/angular-animate/angular-animate.js'=>array('load'=>true, 'check_file'=>false,),
    '/vendor/javascript-extensions/dist/javascript-extensions.min.js'=>array('load'=>true, 'check_file'=>false),
    '/vendor/slick-carousel/slick/slick.min.js'=>array('load'=>true, 'check_file'=>false,),
    '/vendor/magnific-popup/dist/jquery.magnific-popup.min.js' => array ('load' => true, 'check_file' => false),
    '/vendor/ng-dialog/js/ngDialog.min.js'=>array('load'=>true, 'check_file'=>false,),
    '/vendor/scrollmagic/scrollmagic/minified/ScrollMagic.min.js' => array('load' => true, 'check_file' => false,),
    '/vendor/scrollmagic/scrollmagic/minified/plugins/animation.velocity.min.js' => array('load' => true, 'check_file' => false,),
    '/vendor/scrollmagic/scrollmagic/minified/plugins/debug.addIndicators.min.js' => array('load' => true, 'check_file' => false,),
    '/vendor/angular-custom-dropdown-v2/js/dropdowns.js'=>array('load'=>true, 'check_file'=>false),
    '/vendor/imagesloaded/imagesloaded.pkgd.min.js' => array('load' => true, 'check_file' => false,),
    '/vendor/sweetalert/dist/sweetalert.min.js'=>array('load'=>true, 'check_file'=>false),
);


return array(
    'mvc_asset_loader' => array(
        'enabled' => true,
        'cache'=>array(
            'enabled'=>false,
            'cache_path'=>'data/cache/assets',
        ),
        'error'=>array(
            'module'=>'application',
            'controller'=>'index',
            'action'=>'index'
        ),
        'css'=>array(
            'mvc'=>array(
                'application'=>array(
                    'libraries' => $commonCssLibraries
                )
            ),
        ),


        'javascript'=>array(
            'mvc'=>array(
                'application'=>array(
                    'libraries' => $commonJsLibraries
                )
            ),
        ),

    ),
);
