<?php
/**
 * Created by PhpStorm.
 * User: derekmiranda
 * Date: 7/23/14
 * Time: 6:59 PM
 */
return array(
    /**
     * Mail Config
     */
    'mail'=>array(
        'enabled'=>true,
        'override'=>array(
            'enabled'=>false,
        ),
        'connection'=>array(
            'name' => 'outlook.office365.com',
            'host' => 'outlook.office365.com',
            'port' => 587,
            'connection_class'=>'login',
            'connection_config'=>array(
                'username' => 'noreply@greenchiprecycling.com',
                'password' => 'Was70783',
                'ssl' => 'tls'
            ),
        ),
        'layouts'=>array(
            'html'=>'layout/email/html',
            'text'=>'layout/email/text',
        ),
        'configs'=>array(
            'defaults'=>array(
                'to'=>array(
                    'info@greenchiprecycling.com'
                ),
                'from'=>'noreply@greenchiprecycling.com',
            ),
            'inquiry'=>array(
                'subject'=>'Quick Form Inquiry',
                'html_template' => 'inquiry.phtml',
            ), 
            'contact'=>array(
                'subject'=>'Contact Form Inquiry',
                'html_template' => 'contact.phtml',
            ),
            'e-waste-form'=>array(
                'subject'=>'E-Waste Quote Inquiry',
                'html_template' => 'e-waste.phtml',
            ),
        ),
    ),

    /**
     * View Manager
     */
    'view_manager' => array(
        'template_map'=>array(
            'layout/email/html' => __DIR__ . '/../../module/Application/email/layout/html.phtml',
            'layout/email/text' => __DIR__ . '/../../module/Application/email/layout/text.txt'
        ),
        'template_path_stack' => array(
            __DIR__ . '/../../module/Application/email',
        ),
    ),
);
