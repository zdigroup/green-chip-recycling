<?php
/**
 * ZDI Design Group
 * Project lyfeping-api
 * Author derekmiranda
 * Date: 2/16/17 10:45 AM
 *
 * (c) Copyright 2017 derekmiranda | All Rights Reserved
 */
return [
    'zf-mvc-auth' => [
        'authentication' => [
            'adapters' => [
                'oauth2' => [
                    'adapter' => \ZF\MvcAuth\Authentication\OAuth2Adapter::class,
                    'storage' => [
                        'adapter' => 'mongo',
                        'dsn' => 'localhost:27017',
                        'database' => 'green_chip',
                        'route' => '/api/oauth',
                        'options' => [
                            'db' => 'mvs',
                        ],
                    ],
                ],
            ],
        ],
    ],
    'zf-oauth2' => array(
        'options' => array(
            'allow_implicit' => true,
            'access_lifetime' => 86400 * 30, // Seconds (3600 == 1 Hour),
            'always_issue_new_refresh_token' => true,
        ),
    ),
];