<?php
/**
 * ZDI Design Group
 * Project access-medicare
 * Author derekmiranda
 * Date: 10/10/15 7:32 PM
 *
 * (c) Copyright 2015 derekmiranda | All Rights Reserved
 */
return array(
    'mongo' => array(
        'adapters' => array(
            'DefaultMongoAdapter' => array(
                'db' => 'green_chip_testing',
            ),
        ),
    ),
);