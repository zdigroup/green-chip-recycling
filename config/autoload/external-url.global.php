<?php

return array(
    'external-url'=>array(
        'links'=>array(
            'social'=>array(
                'facebook' => 'https://www.facebook.com/Green-Chip-Electronic-Waste-Solutions-292790910836333/',
                'twitter' => 'https://twitter.com/GreenChipNY',
                'linkedin' => 'https://www.linkedin.com/in/bill-monteleone-2394a657'
            ),
            'badges'=>array(
                'r2'=>'https://sustainableelectronics.org/'
            ),
            'map-astoria'=>'https://www.google.com/maps/place/19-36+38th+St,+Astoria,+NY+11105/@40.7779715,-73.9021382,17z/data=!4m5!3m4!1s0x89c25f6357c73265:0xdf25cd1a8886aed3!8m2!3d40.7781096!4d-73.9013282?hl=en',
            'map-brooklyn'=>'https://www.google.com/maps/place/540+Kingsland+Ave,+Brooklyn,+NY+11222/@40.7355207,-73.9465801,17z/data=!3m1!4b1!4m5!3m4!1s0x89c2593125d17f4d:0x8a89af518caa2ca0!8m2!3d40.7355207!4d-73.9443861?hl=en',
            'map-virginia'=>'https://www.google.com/maps/place/GreenChip+Ewaste+%26+ITAD+Solutions/@38.2846229,-77.47325,18.67z/data=!4m13!1m7!3m6!1s0x89b6c1888ad25291:0xcc8d26220249aef2!2s10+Harkness+Blvd,+Fredericksburg,+VA+22401!3b1!8m2!3d38.2845873!4d-77.4731549!3m4!1s0x89b6c1b1e797d803:0xad58ff3745c13f05!8m2!3d38.2845873!4d-77.4731549?hl=en',
            'mail'=>'mailto:info@greenchiprecycling.com',
            'phone-virginia'=>'703-565-9928',
            'phone-florida'=>'954-653-8310',
            'phone-toll-free'=>'844-783-0443',
            'phone'=>'718-349-2620',
            'phone-emergency'=>'845-796-6527',
            'fax'=>'718-504-4999',
            'zdi'=>'https://zdi.rocks',
        ),
    ),
);
