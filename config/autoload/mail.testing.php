<?php
/**
 * Created by PhpStorm.
 * User: derekmiranda
 * Date: 7/23/14
 * Time: 6:59 PM
 */
return array(

    /**
     * Mail Config
     *
     * Override Email Recipients for Development server only
     */
    'mail'=>array(
        'override'=>array(
            'enabled'=>true,
            'address'=>array(
                'bryan@zdidesign.com'
            )
        ),
    ),
);
