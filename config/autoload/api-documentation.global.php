<?php

return [
    'api-documentation'=>[
        'layout'=>'application/layout/api-documentation'
    ],
    'view_manager' => [
        'template_map' => [
            'application/layout/api-documentation'=> __DIR__ . '/../../module/Application/view/layout/api-documentation.phtml',
        ]
    ],
];