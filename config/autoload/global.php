<?php
return [
    'build' => '{BUILD}',
    'version' => '{VERSION}',
    'zf-content-negotiation' => [
        'selectors' => [],
    ],
    'router' => [
        'routes' => [
            'oauth' => [
                'options' => [
                    'spec' => '%oauth%',
                    'regex' => '(?P<oauth>(/api/oauth))',
                ],
                'type' => 'regex',
            ],
        ],
    ],
    'zf-mvc-auth' => [
        'authentication' => [
            'map' => [
                'AdminModule\\V1' => 'oauth2',
                'MailingListApi\\V1' => 'oauth2',
                'DonateApi\\V1' => 'oauth2',
                'ContactApi\\V1' => 'oauth2',
            ],
            'adapters' => [],
        ],
    ],
];
