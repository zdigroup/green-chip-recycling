<?php
/**
 * ZDI Design Group
 * Project lyfeping-api
 * Author derekmiranda
 * Date: 2/16/17 10:45 AM
 *
 * (c) Copyright 2017 derekmiranda | All Rights Reserved
 */
return [
    'zf-mvc-auth' => [
        'authentication' => [
            'adapters' => [
                'oauth2' => [
                    'storage' => [
                        'dsn' => 'db:27017',
                        'database' => 'green_chip',
                        'options' => [
                            'db' => 'green_chip',
                        ],
                    ],
                ],
            ],
        ],
    ],
];
