/**
 * Require Extensions
 */
require('./public/vendor/javascript-extensions/dist/date-extensions.min.js');

/**
 * Set the project name
 * @type {string}
 */
var projectName = 'Green Chip';

/**
 * The development deployment server
 * @type {string}
 */
var developmentServer = '69.87.221.69';

/**
 * The live deployment Server
 * @type {string}
 */
var liveServer = '107.191.36.139';

/**
 * The application path on production.zdidesign.com or server set in developmentServer
 */
var developmentPath = '/var/www/green-chip-testing';

/**
 * The application path on www.zdidesign.com or server set in liveServer
 */
var livePath = '/var/www/green-chip';

/**
 * Set the Application Major Version
 * @type {string}
 */
var version = '1.0';

/**
 * Set the Build Version
 */
var date = new Date();
var buildVersion = date.format('Ymd.His');

/**
 * Output Banner
 */
console.log('--------------------------------------------------------------------');
console.log('Building Version ' + version + ' Build ' + buildVersion + ' of ' + projectName);
console.log('--------------------------------------------------------------------');

module.exports = function(grunt) {

    grunt.initConfig({

        // ******************************
        //
        // Minification of Javascript
        //
        // ******************************
        uglify: {
            //
            // Run During the Development process.
            //
            development: {
                expand: true,
                cwd: 'public/js',
                src: '**/*.js',
                dest: 'public/js',
                options: {
                    mangle: true,
                    preserveComments: false,
                    compress:true,
                    sourceMap:true,
                }
            },
        },

        // ******************************
        //
        // Minification of CSS.
        //
        // ******************************
        cssmin:{
            //
            // Run during any 'deploy' command
            //
            deploy:{
                files: [{
                    expand: true,
                    cwd: 'build/public/css',
                    src: ['**/*.css', '!**/*.min.css'],
                    dest: 'build/public/css',
                    ext: '.css'
                }]
            }
        },

        // ******************************
        //
        // Clean Directories
        //
        // ******************************
        clean: ["build"],

        // ******************************
        //
        // Copy Files
        //
        // ******************************
        copy:{
            //
            // Run during any 'deploy' command
            //
            "deploy-development":{
                files:[
                    {
                        expand: true,
                        flatten: false,
                        src: [
                            'db/**',
                            'module/**',
                            'data/**',
                            '!vendor/**',
                            'config/**',
                            '!config/**/*.development.php',
                            '!config/**/*.local.php',
                            '!config/**/*.me.php',
                            '!config/development.config.php',
                            '!config/modules.config.old',
                            'public/**/*',
                            '!public/**/*.scss',
                            '!public/**/*.less',
                            '!public/css/src/**/*',
                            '!public/css/partials/**/*',
                            '!**/*.psd',
                            'public/.htaccess',
                            'schema/**/*',
                            'src/**/*'
                        ],
                        dest: 'build'
                    }
                ]
            },
            "deploy-live":{
                files:[
                    {
                        expand: true,
                        flatten: false,
                        src: [
                            'db/**',
                            'module/**',
                            'data/**',
                            '!vendor/**',
                            'config/**',
                            '!config/**/*.testing.php',
                            '!config/**/*.development.php',
                            '!config/**/*.local.php',
                            '!config/**/*.me.php',
                            '!config/development.config.php',
                            '!config/*-development.php',
                            '!config/modules.config.old',
                            'public/**/*',
                            '!public/**/*.scss',
                            '!public/**/*.less',
                            '!public/css/src/**/*',
                            '!public/css/partials/**/*',
                            '!**/*.psd',
                            'public/.htaccess',
                            'schema/**/*',
                            'src/**/*'
                        ],
                        dest: 'build'
                    }
                ]
            },
            development:{
                files:[
                    {
                        expand: true,
                        flatten: false,
                        cwd: 'module/Admin/public/templates',
                        src: [
                            '**/*.html'
                        ],
                        dest: 'public/templates/admin'
                    },
                    {
                        expand: true,
                        flatten: false,
                        cwd: 'module/Application/public/templates',
                        src: [
                            '**/*',
                        ],
                        dest: 'public/templates/application'
                    }
                ]
            },
        },

        // ******************************
        //
        // SSH Copy to Server
        //
        // ******************************
        rsync: {
            //
            // Global Options for Copy
            //
            options: {
                args: ['--chmod=ugo=rwX', '--perms=off', '--include=".htaccess"'],
                exclude: [".git*","*.scss","node_modules"],
                recursive: true
            },
            //
            // Run during live deployment
            //
            live: {
                options: {
                    src: ["build/"],
                    dest: livePath,
                    host: "<%= grunt.option('username') %>@" + liveServer,
                    //delete: true // Careful this option could cause data loss, read the docs!
                }
            },
            //
            // Run during development deployment
            //
            development: {
                options: {
                    src: ["build/"],
                    dest: developmentPath,
                    host: "<%= grunt.option('username') %>@" + developmentServer,
                    //delete: true // Careful this option could cause data loss, read the docs!
                }
            }
        },

        // ******************************
        //
        // Compile SCSS
        //
        // ******************************
        sass: {
            //
            // Run during development
            //
            development: {
                options: {
                    style: 'expanded',
                    noCache:false,
                    update:true,
                    loadPath:[
                        'public/vendor/'
                    ]
                },
                files:[
                    {
                        expand: true,
                        cwd: 'module/Application/public/scss',
                        src: ['**/*.scss','!src/**/*', '!partials/**/*', '!**/partials/**/*', '!**/src/**/*'],
                        dest: 'public/css',
                        ext: '.css'
                    },
                    {
                        expand: true,
                        cwd: 'module/Admin/public/scss',
                        src: ['**/*.scss','!src/**/*', '!partials/**/*', '!**/partials/**/*', '!**/src/**/*'],
                        dest: 'public/css/admin-module',
                        ext: '.css'
                    }
                ]
            },
            //
            // Run during any 'deploy' command
            //
            'deploy': {
                options: {
                    style: 'expanded',
                    noCache:false,
                    update:true,
                    loadPath:[
                        'public/vendor/'
                    ]
                },
                files:[
                    {
                        expand: true,
                        cwd: 'module/Application/public/scss',
                        src: ['**/*.scss','!src/**/*', '!partials/**/*', '!**/partials/**/*', '!**/src/**/*'],
                        dest: 'build/public/css',
                        ext: '.css'
                    }
                ]
            },
        },

        // ******************************
        //
        // Concatenate JS files into one bundle
        //
        // ******************************
        concat: {
            //
            // Run during any 'deploy' command
            //
            development: {
                files:{
                    'public/js/application.js': [
                        'module/Application/public/js/src/*.js',
                        'module/Application/public/js/src/**/*.js',
                        'module/Application/public/js/application.js',
                        'module/Application/public/js/application/**/*.js',
                        'module/Application/public/js/application/*.js',
                    ],
                    'public/js/admin-module.js': [
                        'module/Admin/public/js/**/*.js',
                    ]
                }
            }
        },

        // ******************************
        //
        // Concurrent Watcher
        //
        // ******************************
        concurrent: {
            //
            // Run during developmentl; watches SCSS and JS independently
            development: {
                tasks: ['watch:sass', 'watch:js', 'watch:templates'],
                options: {
                    logConcurrentOutput: true
                }
            }
        },

        // ******************************
        //
        // String Replace
        //
        // ******************************
        'string-replace': {
            //
            // Sets the build number in the configuration
            //
            build: {
                files: [
                    {
                        expand: true,
                        cwd: 'build/config/autoload',
                        src: 'global.php',
                        dest: 'build/config/autoload',
                    }],
                options: {
                    replacements: [
                        {
                            pattern:'{BUILD}',
                            replacement:buildVersion
                        },
                        {
                            pattern:'{VERSION}',
                            replacement:version
                        }
                    ]
                }
            },
            //
            // Sets the Application Environment State to "testing" in .htaccess
            // Run during deploy development
            //
            development: {
                files: [
                    {
                        expand: true,
                        cwd: 'build/public',
                        src: ['.htaccess'],
                        dest: 'build/public',
                    }],
                options: {
                    replacements: [
                        {
                            pattern:'SetEnv "ApplicationEnvironment" "development"',
                            replacement:'SetEnv "ApplicationEnvironment" "testing"'
                        }
                    ]
                }
            },
            //
            // Sets the Application Environment State to "production" in .htaccess
            // Run during deploy live
            //
            live: {
                files: [
                    {
                        expand: true,
                        cwd: 'build/public',
                        src: ['.htaccess'],
                        dest: 'build/public',
                    }],
                options: {
                    replacements: [
                        {
                            pattern:'SetEnv "ApplicationEnvironment" "development"',
                            replacement:'SetEnv "ApplicationEnvironment" "production"'
                        }
                    ]
                }
            }
        },

        // ******************************
        //
        // File Watchers
        //
        // ******************************
        watch: {
            //
            // Listens for changes to SCSS files
            //
            sass: {
                files: [
                    'module/Application/public/scss/**/*.scss',
                    'module/Admin/public/scss/**/*.scss',
                ],
                tasks: ['string-replace:build', 'sass:development'],
                options: {
                    interrupt: true,
                },
            },
            //
            // Listens for changes to JS files
            //
            js:{
                files: [
                    'module/Application/public/js/**/*',
                    'module/Admin/public/js/**/*',
                ],
                tasks: ['string-replace:build', 'concat:development', 'copy:development', 'uglify:development'],
                options: {
                    interrupt: true,
                },
            },
            templates:{
                files: [
                    'module/Admin/public/templates/**/*',
                    'module/Application/public/templates/**/*'
                ],
                tasks: ['copy:development'],
                options: {
                    interrupt: true,
                },
            }
        },

        // ******************************
        //
        // File Watchers
        //
        // ******************************
        exec: {
            composerPrepare: {
                stderr: true,
                stdout: true,
                command: 'composer dump-autoload --optimize'
            },
            composerUpdate: {
                stderr: true,
                stdout: true,
                command: 'composer update --ignore-platform-reqs'
            },
            bowerUpdate: {
                stderr: true,
                stdout: true,
                command: 'bower update --f'
            },
            installComponents: {
                stderr: true,
                stdout: true,
                command: 'sh vendor/spectravp/admin-module/install.sh'
            },
            startPhantomJs:{
                stderr:true,
                stdout:true,
                command:'phantomjs --webdriver=4444 &'
            },
            runUnitTests: {
                stderr:true,
                stdout:true,
                command: 'codecept run'
            }
        },
    });

    /**
     * Register Required NPM Modules
     */
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-concurrent');
    grunt.loadNpmTasks('grunt-rsync');
    grunt.loadNpmTasks('grunt-string-replace');
    grunt.loadNpmTasks('grunt-exec');

    /**
     * Run the Deployment Task
     *
     * @run: grunt update
     */
    grunt.registerTask('update', [
        'exec:composerUpdate',
        'exec:composerPrepare',
        'exec:installComponents'
    ]);

    /**
     * Run the Deployment Task
     *
     * @run: grunt deploy --target development|live --username xxxxx
     */
    grunt.registerTask('deploy', [
        'update',
        'exec:runUnitTests',
        'deploy-fast'
    ]);

    /**
     * Run the Deployment Task
     *
     * @run: grunt deploy --target development|live --username xxxxx
     */
    grunt.registerTask('deploy-fast', [
        'build',
        'rsync:'+ grunt.option('target') || 'development',
        'clean'
    ]);

    /**
     * Run the Build Task
     * This is the same as deploy except it doesn't deploy the application
     *
     * @run: grunt build --target development|live
     */

    grunt.registerTask('build', [
        'clean',
        'sass:development',
        'concat:development',
        'uglify:development',
        'copy:deploy-' + grunt.option('target') || 'development',
        'sass:deploy',
        'string-replace:build',
        'string-replace:' + grunt.option('target') || 'development',
        'cssmin:deploy'
    ]);

    /**
     * Builds only the js
     * @run: grunt build-js
     */
    grunt.registerTask('build-js', [
        'concat:development',
        'uglify:development'
    ]);

    /**
     * Run the Development Watcher
     * This is run while you are coding
     *
     * @run grunt development
     */
    grunt.registerTask('development', [
        'update',
        'development-fast',
       ]);

    /**
     * Run codeception
     *
     * @run grunt codeception
     */

    grunt.registerTask('codeception', [
        'exec:startPhantomJs'
    ]);

    /**
     * Run the Development Watcher
     * This is run while you are coding
     *
     * @run grunt development
     */
    grunt.registerTask('development-fast', [
        'sass:development',
        'concat:development',
        'copy:development',
        'uglify:development',
        'concurrent:development'
        ]);
};

// mongodump --db response --out /var/www/schema
// rsync -chavzP --stats spectravp@production-14.zdidesign.com:/var/www/mongodb/response /Volumes/Environments/response/schema
// rsync -chavzP --stats spectravp@production-14.zdidesign.com:/var/www/response/public/assets /Volumes/Environments/response/public

// PULL DOWN SSL
// rsync -chavzP --stats spectravp@69.87.221.45:/var/www/ssl /Volumes/Environments/response
