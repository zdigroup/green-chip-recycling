<?php
return [
    'mongo' => [
        'collections' => [
            'InquiryCollection' => 'inquiry',
            'ContactCollection' => 'contact',
            'WasteCollection' => 'waste',
        ],
    ],
    'service_manager' => [
        'factories' => [
            \FormsApi\V1\Rest\Inquiry\InquiryResource::class => \FormsApi\V1\Rest\Inquiry\InquiryResourceFactory::class,
            \FormsApi\V1\Rest\Contact\ContactResource::class => \FormsApi\V1\Rest\Contact\ContactResourceFactory::class,
            \FormsApi\V1\Rest\Waste\WasteResource::class => \FormsApi\V1\Rest\Waste\WasteResourceFactory::class,
        ],
    ],
    'router' => [
        'routes' => [
            'forms-api.rest.inquiry' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/api/forms/inquiry[/:inquiry_id]',
                    'defaults' => [
                        'controller' => 'FormsApi\\V1\\Rest\\Inquiry\\Controller',
                    ],
                ],
            ],
            'forms-api.rest.contact' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/api/forms/contact[/:contact_id]',
                    'defaults' => [
                        'controller' => 'FormsApi\\V1\\Rest\\Contact\\Controller',
                    ],
                ],
            ],
            'forms-api.rest.waste' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/api/forms/waste[/:waste_id]',
                    'defaults' => [
                        'controller' => 'FormsApi\\V1\\Rest\\Waste\\Controller',
                    ],
                ],
            ],
            'forms-api.rpc.upload-files' => [
                'type' => 'Segment',
                'options' => [
                    'route' => '/api/forms/upload-files',
                    'defaults' => [
                        'controller' => 'FormsApi\\V1\\Rpc\\UploadFiles\\Controller',
                        'action' => 'uploadFiles',
                    ],
                ],
            ],
        ],
    ],
    'zf-versioning' => [
        'uri' => [
            0 => 'forms-api.rest.inquiry',
            1 => 'forms-api.rest.contact',
            2 => 'forms-api.rest.waste',
            3 => 'forms-api.rpc.upload-files',
        ],
    ],
    'zf-rest' => [
        'FormsApi\\V1\\Rest\\Inquiry\\Controller' => [
            'listener' => \FormsApi\V1\Rest\Inquiry\InquiryResource::class,
            'route_name' => 'forms-api.rest.inquiry',
            'route_identifier_name' => 'inquiry_id',
            'collection_name' => 'inquiry',
            'entity_http_methods' => [
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ],
            'collection_http_methods' => [
                0 => 'GET',
                1 => 'POST',
            ],
            'collection_query_whitelist' => [
                0 => 'sort',
                1 => 'search',
                2 => 'direction',
            ],
            'page_size' => 25,
            'page_size_param' => 'pageSize',
            'entity_class' => \FormsApi\V1\Rest\Inquiry\InquiryEntity::class,
            'collection_class' => \FormsApi\V1\Rest\Inquiry\InquiryCollection::class,
            'service_name' => 'Inquiry',
        ],
        'FormsApi\\V1\\Rest\\Contact\\Controller' => [
            'listener' => \FormsApi\V1\Rest\Contact\ContactResource::class,
            'route_name' => 'forms-api.rest.contact',
            'route_identifier_name' => 'contact_id',
            'collection_name' => 'contact',
            'entity_http_methods' => [
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ],
            'collection_http_methods' => [
                0 => 'GET',
                1 => 'POST',
            ],
            'collection_query_whitelist' => [
                0 => 'sort',
                1 => 'search',
                2 => 'direction',
            ],
            'page_size' => 25,
            'page_size_param' => 'pageSize',
            'entity_class' => \FormsApi\V1\Rest\Contact\ContactEntity::class,
            'collection_class' => \FormsApi\V1\Rest\Contact\ContactCollection::class,
            'service_name' => 'Contact',
        ],
        'FormsApi\\V1\\Rest\\Waste\\Controller' => [
            'listener' => \FormsApi\V1\Rest\Waste\WasteResource::class,
            'route_name' => 'forms-api.rest.waste',
            'route_identifier_name' => 'waste_id',
            'collection_name' => 'waste',
            'entity_http_methods' => [
                0 => 'GET',
                1 => 'PATCH',
                2 => 'PUT',
                3 => 'DELETE',
            ],
            'collection_http_methods' => [
                0 => 'GET',
                1 => 'POST',
            ],
            'collection_query_whitelist' => [
                0 => 'sort',
                1 => 'search',
                2 => 'direction',
            ],
            'page_size' => 25,
            'page_size_param' => 'pageSize',
            'entity_class' => \FormsApi\V1\Rest\Waste\WasteEntity::class,
            'collection_class' => \FormsApi\V1\Rest\Waste\WasteCollection::class,
            'service_name' => 'Waste',
        ],
    ],
    'zf-content-negotiation' => [
        'controllers' => [
            'FormsApi\\V1\\Rest\\Inquiry\\Controller' => 'HalJson',
            'FormsApi\\V1\\Rest\\Contact\\Controller' => 'HalJson',
            'FormsApi\\V1\\Rest\\Waste\\Controller' => 'HalJson',
            'FormsApi\\V1\\Rpc\\UploadFiles\\Controller' => 'Json',
        ],
        'accept_whitelist' => [
            'FormsApi\\V1\\Rest\\Inquiry\\Controller' => [
                0 => 'application/vnd.forms-api.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ],
            'FormsApi\\V1\\Rest\\Contact\\Controller' => [
                0 => 'application/vnd.forms-api.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ],
            'FormsApi\\V1\\Rest\\Waste\\Controller' => [
                0 => 'application/vnd.forms-api.v1+json',
                1 => 'application/hal+json',
                2 => 'application/json',
            ],
            'FormsApi\\V1\\Rpc\\UploadFiles\\Controller' => [
                0 => 'application/vnd.forms-api.v1+json',
                1 => 'application/json',
                2 => 'application/*+json',
            ],
        ],
        'content_type_whitelist' => [
            'FormsApi\\V1\\Rest\\Inquiry\\Controller' => [
                0 => 'application/vnd.forms-api.v1+json',
                1 => 'application/json',
            ],
            'FormsApi\\V1\\Rest\\Contact\\Controller' => [
                0 => 'application/vnd.forms-api.v1+json',
                1 => 'application/json',
            ],
            'FormsApi\\V1\\Rest\\Waste\\Controller' => [
                0 => 'application/vnd.forms-api.v1+json',
                1 => 'application/json',
            ],
            'FormsApi\\V1\\Rpc\\UploadFiles\\Controller' => [
                0 => 'application/vnd.forms-api.v1+json',
                1 => 'application/json',
                2 => 'multipart/form-data',
            ],
        ],
    ],
    'zf-hal' => [
        'metadata_map' => [
            \FormsApi\V1\Rest\Inquiry\InquiryEntity::class => [
                'entity_identifier_name' => '_id',
                'route_name' => 'forms-api.rest.inquiry',
                'route_identifier_name' => 'inquiry_id',
                'hydrator' => \Zend\Hydrator\ArraySerializable::class,
            ],
            \FormsApi\V1\Rest\Inquiry\InquiryCollection::class => [
                'entity_identifier_name' => '_id',
                'route_name' => 'forms-api.rest.inquiry',
                'route_identifier_name' => 'inquiry_id',
                'is_collection' => true,
            ],
            \FormsApi\V1\Rest\Contact\ContactEntity::class => [
                'entity_identifier_name' => '_id',
                'route_name' => 'forms-api.rest.contact',
                'route_identifier_name' => 'contact_id',
                'hydrator' => \Zend\Hydrator\ArraySerializable::class,
            ],
            \FormsApi\V1\Rest\Contact\ContactCollection::class => [
                'entity_identifier_name' => '_id',
                'route_name' => 'forms-api.rest.contact',
                'route_identifier_name' => 'contact_id',
                'is_collection' => true,
            ],
            \FormsApi\V1\Rest\Waste\WasteEntity::class => [
                'entity_identifier_name' => '_id',
                'route_name' => 'forms-api.rest.waste',
                'route_identifier_name' => 'waste_id',
                'hydrator' => \Zend\Hydrator\ArraySerializable::class,
            ],
            \FormsApi\V1\Rest\Waste\WasteCollection::class => [
                'entity_identifier_name' => '_id',
                'route_name' => 'forms-api.rest.waste',
                'route_identifier_name' => 'waste_id',
                'is_collection' => true,
            ],
        ],
    ],
    'zf-content-validation' => [
        'FormsApi\\V1\\Rest\\Inquiry\\Controller' => [
            'input_filter' => 'FormsApi\\V1\\Rest\\Inquiry\\Validator',
        ],
        'FormsApi\\V1\\Rest\\Contact\\Controller' => [
            'input_filter' => 'FormsApi\\V1\\Rest\\Contact\\Validator',
        ],
        'FormsApi\\V1\\Rest\\Waste\\Controller' => [
            'input_filter' => 'FormsApi\\V1\\Rest\\Waste\\Validator',
        ],
        'FormsApi\\V1\\Rpc\\UploadFiles\\Controller' => [
            'input_filter' => 'FormsApi\\V1\\Rpc\\UploadFiles\\Validator',
        ],
    ],
    'input_filter_specs' => [
        'FormsApi\\V1\\Rest\\Inquiry\\Validator' => [
            0 => [
                'required' => true,
                'validators' => [],
                'filters' => [
                    0 => [
                        'name' => \Filter\Sanitize::class,
                        'options' => [],
                    ],
                ],
                'name' => 'category',
                'description' => 'Category of recycling collection wanted',
            ],
            1 => [
                'required' => true,
                'validators' => [],
                'filters' => [
                    0 => [
                        'name' => \Filter\Sanitize::class,
                        'options' => [],
                    ],
                ],
                'name' => 'location',
                'description' => 'Location of collection (office, etc)',
            ],
            2 => [
                'required' => true,
                'validators' => [],
                'filters' => [
                    0 => [
                        'name' => \Filter\Sanitize::class,
                        'options' => [],
                    ],
                ],
                'name' => 'address',
                'description' => 'User\'s address',
            ],
            3 => [
                'required' => true,
                'validators' => [],
                'filters' => [
                    0 => [
                        'name' => \Filter\Sanitize::class,
                        'options' => [],
                    ],
                ],
                'name' => 'phone',
                'description' => 'User\'s phone number',
            ],
            4 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Zend\Validator\EmailAddress::class,
                        'options' => [
                            'message' => 'Email is invalid',
                        ],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Filter\Sanitize::class,
                        'options' => [],
                    ],
                ],
                'name' => 'email',
                'description' => 'User\'s email',
            ],
            5 => [
                'required' => true,
                'validators' => [],
                'filters' => [],
                'name' => 'newsletter_opt_in',
                'description' => 'User\'s option to sign up for the newsletter',
                'allow_empty' => true,
            ],
        ],
        'FormsApi\\V1\\Rest\\Contact\\Validator' => [
            0 => [
                'required' => true,
                'validators' => [],
                'filters' => [
                    0 => [
                        'name' => \Filter\Sanitize::class,
                        'options' => [],
                    ],
                ],
                'name' => 'name',
                'description' => 'User\'s name',
            ],
            1 => [
                'required' => true,
                'validators' => [
                    0 => [
                        'name' => \Zend\Validator\EmailAddress::class,
                        'options' => [
                            'message' => 'Email is invalid',
                        ],
                    ],
                ],
                'filters' => [
                    0 => [
                        'name' => \Filter\Sanitize::class,
                        'options' => [],
                    ],
                ],
                'name' => 'email',
                'description' => 'User\'s email',
            ],
            2 => [
                'required' => true,
                'validators' => [],
                'filters' => [
                    0 => [
                        'name' => \Filter\Sanitize::class,
                        'options' => [],
                    ],
                ],
                'name' => 'category',
                'description' => 'Category of collection requested',
            ],
            3 => [
                'required' => true,
                'validators' => [],
                'filters' => [
                    0 => [
                        'name' => \Filter\Sanitize::class,
                        'options' => [],
                    ],
                ],
                'name' => 'location',
                'description' => 'Location for collection (office, etc)',
            ],
            4 => [
                'required' => true,
                'validators' => [],
                'filters' => [
                    0 => [
                        'name' => \Filter\Sanitize::class,
                        'options' => [],
                    ],
                ],
                'name' => 'address',
                'description' => 'User\'s address',
            ],
            5 => [
                'required' => false,
                'validators' => [],
                'filters' => [
                    0 => [
                        'name' => \Filter\Sanitize::class,
                        'options' => [],
                    ],
                ],
                'name' => 'comments',
                'description' => 'User\'s comments',
            ],
            6 => [
                'required' => true,
                'validators' => [],
                'filters' => [
                    0 => [
                        'name' => \Filter\Sanitize::class,
                        'options' => [],
                    ],
                ],
                'name' => 'phone',
                'description' => 'User\'s Phone',
            ],
            7 => [
                'required' => true,
                'validators' => [],
                'filters' => [],
                'name' => 'newsletter_opt_in',
                'description' => 'User\'s option to sign up for the newsletter',
                'allow_empty' => true,
            ],
        ],
        'FormsApi\\V1\\Rest\\Waste\\Validator' => [
            0 => [
                'required' => false,
                'validators' => [],
                'filters' => [
                    0 => [
                        'name' => \Filter\Sanitize::class,
                        'options' => [],
                    ],
                ],
                'allow_empty'=> true,
                'name' => 'uploadedFiles',
                'description' => 'User\'s uploaded files',
            ],
            1 => [
                'required' => true,
                'validators' => [],
                'filters' => [
                    0 => [
                        'name' => \Filter\Sanitize::class,
                        'options' => [],
                    ],
                ],
                'name' => 'requiredServices',
            ],
        ],
        'FormsApi\\V1\\Rpc\\UploadFiles\\Validator' => [
            0 => [
                'required' => true,
                'validators' => [],
                'filters' => [
                    0 => [
                        'name' => \Filter\File\RenameUpload::class,
                        'options' => [
                            'strip_public' => true,
                            'overwrite' => true,
                            'use_upload_extension' => true,
                            'use_upload_name' => true,
                            'auto_create_directory' => true,
                            'target' => 'public/files',
                        ],
                    ],
                ],
                'name' => 'files',
                'type' => \Zend\InputFilter\FileInput::class,
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            'FormsApi\\V1\\Rpc\\UploadFiles\\Controller' => \FormsApi\V1\Rpc\UploadFiles\UploadFilesControllerFactory::class,
        ],
    ],
    'zf-rpc' => [
        'FormsApi\\V1\\Rpc\\UploadFiles\\Controller' => [
            'service_name' => 'UploadFiles',
            'http_methods' => [
                0 => 'POST',
            ],
            'route_name' => 'forms-api.rpc.upload-files',
        ],
    ],
];
