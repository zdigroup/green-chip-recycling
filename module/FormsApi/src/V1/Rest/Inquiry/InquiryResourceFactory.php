<?php
namespace FormsApi\V1\Rest\Inquiry;

class InquiryResourceFactory
{
    public function __invoke($services)
    {
        return new InquiryResource($services);
    }
}
