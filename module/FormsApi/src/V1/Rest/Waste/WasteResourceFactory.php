<?php
namespace FormsApi\V1\Rest\Waste;

class WasteResourceFactory
{
    public function __invoke($services)
    {
        return new WasteResource($services);
    }
}
