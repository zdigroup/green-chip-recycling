<?php
namespace FormsApi\V1\Rest\Waste;

use MongoDB\BSON\Regex;
use MongoDB\BSON\UTCDateTime;
use MongoPackage\Collection\AbstractCollection;
use MongoPackage\Paginator\Adapter\MongoPaginatorAdapter;
use Zend\Stdlib\ArrayUtils;
use ZF\ApiProblem\ApiProblem;
use ZF\Rest\AbstractResourceListener;
use Zend\ServiceManager\ServiceLocatorInterface;

class WasteResource extends AbstractResourceListener
{
    /**
     * @var ServiceLocatorInterface
     */
    protected $serviceLocator;

    /**
     * BasicResource constructor.
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function __construct(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    /**
     * @return ServiceLocatorInterface
     */
    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    /**
     * @param ServiceLocatorInterface $serviceLocator
     */
    public function setServiceLocator($serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    /**
     * @return AbstractCollection
     */
    public function getCollection()
    {
        return $this->getServiceLocator()->get('WasteCollection');
    }


    /**
     * Create a resource
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function create($data)
    {
        $data = $this->getInputFilter()->getValues();
        $data['datetime_created'] = new UTCDateTime();
        $this->getCollection()->insert($data);

        $mail = $this->getServiceLocator()->get('Mail');
        $mail->send('e-waste-form', $data);

        return $data;
    }

    /**
     * Delete a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function delete($id)
    {
        return (bool)$this->getCollection()->remove($id);
    }

    /**
     * Delete a collection, or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function deleteList($data)
    {
        return new ApiProblem(405, 'The DELETE method has not been defined for collections');
    }

    /**
     * Fetch a resource
     *
     * @param  mixed $id
     * @return ApiProblem|mixed
     */
    public function fetch($id)
    {
        return $this->getCollection()->getById($id);
    }

    /**
     * Fetch all or a subset of resources
     *
     * @param  array $params
     * @return ApiProblem|mixed
     */
    public function fetchAll($params = [])
    {
        $sort = array();

        if (isset($params['sort'])) {
            $sort = array($params['sort'] => (int)$params['direction']);
            unset($params['sort']);
            unset($params['direction']);
        }

        if (isset($params['search'])) {
            $searchTerm = $params['search'];
            unset($params['search']);

            $searchQuery = array(
                '$or' => array(
                    array(
                        'name' => new Regex($searchTerm, "i"),
                    ),
                    array(
                        'email' => new Regex($searchTerm, "i"),
                    ),
                    array(
                        'event_type' => new Regex($searchTerm, "i"),
                    ),
                    array(
                        'phone' => new Regex($searchTerm, "i"),
                    ),
                )
            );

            $params = ArrayUtils::merge($params->toArray(), $searchQuery);
        }

        $options = array(
            'sort' => $sort,
        );

        $data = $this->getCollection()->findLater($params, $options);
        return new WasteCollection(new MongoPaginatorAdapter($data));
    }

    /**
     * Patch (partial in-place update) a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patch($id, $data)
    {
        return new ApiProblem(405, 'The PATCH method has not been defined for individual resources');
    }

    /**
     * Patch (partial in-place update) a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function patchList($data)
    {
        return new ApiProblem(405, 'The PATCH method has not been defined for collections');
    }

    /**
     * Replace a collection or members of a collection
     *
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function replaceList($data)
    {
        return new ApiProblem(405, 'The PUT method has not been defined for collections');
    }

    /**
     * Update a resource
     *
     * @param  mixed $id
     * @param  mixed $data
     * @return ApiProblem|mixed
     */
    public function update($id, $data)
    {
        $data = $this->getInputFilter()->getValues();
        $this->getCollection()->update($id, $data);
        return $data;
    }
}
