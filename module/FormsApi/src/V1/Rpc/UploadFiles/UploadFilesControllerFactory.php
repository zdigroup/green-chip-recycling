<?php
namespace FormsApi\V1\Rpc\UploadFiles;

class UploadFilesControllerFactory
{
    public function __invoke($controllers)
    {
        return new UploadFilesController();
    }
}
