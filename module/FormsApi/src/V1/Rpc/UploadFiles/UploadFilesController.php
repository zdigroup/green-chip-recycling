<?php
namespace FormsApi\V1\Rpc\UploadFiles;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;

class UploadFilesController extends AbstractActionController
{
    public function uploadFilesAction()
    {
        $data = $this->getEvent()->getParam('ZF\ContentValidation\InputFilter')->getValues();
        return new JsonModel($data);
    }
}
