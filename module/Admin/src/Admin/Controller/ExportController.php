<?php
/**
 * ZDI Design Group
 * Project mvs
 * Author derekmiranda
 * Date: 1/29/18 12:36 PM
 *
 * (c) Copyright 2018 derekmiranda | All Rights Reserved
 */

namespace Admin\Controller;


use AdvancedActionController\Controller\AbstractActionController;
use MongoDB\BSON\UTCDateTime;
use MongoDB\Model\BSONArray;
use MongoPackage\Collection\AbstractCollection;

class ExportController extends AbstractActionController
{

    public function inquiryListAction()
    {
        $keys = array(
            'category',
            'location',
            'address',
            'email',
            'phone',
            'newsletter_opt_in',
            'datetime_created'
        );

        /**
         * @var $collection AbstractCollection
         */
        $collection = $this->getServiceLocator()->get('InquiryCollection');
        $data = $collection->find();
        $rows = [implode(',', $keys) . PHP_EOL];

        foreach ($data as $row)
        {
            $parts = [];

            foreach ($keys as $key)
            {
                if (!isset($row[$key])) {
                    $row[$key] = '';
                }

                if ($key === 'datetime_created' && isset($row[$key]) && $row[$key] instanceof UTCDateTime) {
                    $row[$key] = $row[$key]->toDateTime()->format('Y-m-d H:i:s');
                }

                $parts[] = '"'.$row[$key] . '"';
            }

            $rows[] = implode(',', $parts) . PHP_EOL;
        }

        $this->sendCsv('Quick-Form-List.csv', implode('', $rows));
    }

    public function contactListAction()
    {
        $keys = array(
            'name',
            'email',
            'phone',
            'category',
            'location',
            'address',
            'comments',
            'newsletter_opt_in',
            'datetime_created'
        );

        /**
         * @var $collection AbstractCollection
         */
        $collection = $this->getServiceLocator()->get('ContactCollection');
        $data = $collection->find();
        $rows = [implode(',', $keys) . PHP_EOL];

        foreach ($data as $row)
        {
            $parts = [];

            foreach ($keys as $key)
            {
                if (!isset($row[$key])) {
                    $row[$key] = '';
                }

                if ($key === 'datetime_created' && isset($row[$key]) && $row[$key] instanceof UTCDateTime) {
                    $row[$key] = $row[$key]->toDateTime()->format('Y-m-d H:i:s');
                }

                $parts[] = '"'.$row[$key] . '"';
            }

            $rows[] = implode(',', $parts) . PHP_EOL;
        }

        $this->sendCsv('Contact-List.csv', implode('', $rows));
    }


    public function ewasteAction()
    {
        $keys = array(
            'name',
            'company',
            'email',
            'phone',
            'whereLocated',
            'floorNumber',
            'loadingDockElevator',
            'ewaste',
            'itad',
            'hardDrive',
            'datetime_created'
        );

        /**
         * @var $collection AbstractCollection
         */
        $collection = $this->getServiceLocator()->get('WasteCollection');
        $data = $collection->find();
        $rows = [implode(',', $keys) . PHP_EOL];

        foreach ($data as $row)
        {
            $parts = [];
            $rowBase = $row['requiredServices']['common']['model'];
            $rowBase['datetime_created'] = $row['datetime_created'];

            if ($row['requiredServices']['ewaste']['selected']) {
                $rowBase['ewaste'] = 'Yes';
            } else {
                $rowBase['ewaste'] = 'No';
            }

            if ($row['requiredServices']['hardDrive']['selected']) {
                $rowBase['hardDrive'] = 'Yes';
            } else {
                $rowBase['hardDrive'] = 'No';
            }

            if ($row['requiredServices']['itad']['selected']) {
                $rowBase['itad'] = 'Yes';
            } else {
                $rowBase['itad'] = 'No';
            }

            foreach ($keys as $key)
            {
                if (!isset($rowBase[$key])) {
                    $rowBase[$key] = '';
                }

                if ($key === 'datetime_created' && isset($rowBase[$key]) && $rowBase[$key] instanceof UTCDateTime) {
                    $rowBase[$key] = $rowBase[$key]->toDateTime()->format('Y-m-d H:i:s');
                }

                $parts[] = '"'.$rowBase[$key] . '"';
            }

            $rows[] = implode(',', $parts) . PHP_EOL;
        }

        $this->sendCsv('E-Waste-List.csv', implode('', $rows));
    }

    protected function sendCsv($fileName, $content)
    {
        $length = strlen($content);

        header('Content-Description: File Transfer');
        header('Content-Type: text/plain');//<<<<
        header('Content-Disposition: attachment; filename=' . $fileName);
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: ' . $length);
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Expires: 0');
        header('Pragma: public');

        echo $content;
        exit;
    }
}
