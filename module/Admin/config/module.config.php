<?php
/**
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3-Clause
 * @copyright Copyright (c) 2014 Zend Technologies USA Inc. (http://www.zend.com)
 */

namespace Admin;
use Admin\Controller\ExportController;
use AdvancedActionController\Factory\ControllerFactory;
use AdvancedActionController\Router\Http\Literal;


return array(
    'admin_module'=>array(
        'assets'=> [
            'css' => [
                '/vendor/font-awesome/css/all.min.css',
                '/vendor/font-awesome/css/v4-shims.min.css'
            ]
        ],
        'admin_name'=>'Green Chip Admin',
        'authentication'=>array(
            'session_namespace'=>'sgs',
            'client_id'=>'applicationAdmin',
            'client_secret'=>'w};X5JjUFu8sZ*ac',
        ),
        'theme'=>array(
            'enabled'=>true,
            'assets'=>array(
                'css'=>array(
                    '/css/admin-module/admin-module.css',
                )
            )
        ),
        'acl'=>array(
            'admin'=>array(
                'admin/inquiry',
                'admin/contact',
                'admin/e-waste',
            ),
        ),
        'header'=>array(
            'links'=>array(
                1 => array(
                    // Label for the Link
                    'label'=>'Forms',
                    // Custom attributes to apply to the li element (optional)
                    'attributes'=>array(),
                    // Icon class to use (optional)
                    'icon'=>'fa fa-user-circle-o',
                    // Route it should link to
                    'route'=>'admin/inquiry',
                    'children'=>array(
                        0 => array(
                            // Label for the Link
                            'label'=>'Inquiry Form',
                            // Custom attributes to apply to the li element (optional)
                            'attributes'=>array(),
                            // Icon class to use (optional)
                            'icon'=>'fa fa-address-book-o',
                            // Route it should link to
                            'route'=>'admin/inquiry',
                        ),
                        1 => array(
                            // Label for the Link
                            'label'=>'Contact Form',
                            // Custom attributes to apply to the li element (optional)
                            'attributes'=>array(),
                            // Icon class to use (optional)
                            'icon'=>'fa fa-user-circle-o',
                            // Route it should link to
                            'route'=>'admin/contact',
                        ),
                        2 => array(
                            // Label for the Link
                            'label'=>'E-Waste Form',
                            // Custom attributes to apply to the li element (optional)
                            'attributes'=>array(),
                            // Icon class to use (optional)
                            'icon'=>'fa fa-user-circle-o',
                            // Route it should link to
                            'route'=>'admin/e-waste',
                        )
                    )
                ),
            ),
        ),
    ),
    'router' => array(
        'routes' => array(
            'admin' => array(
                'child_routes'=>array(
                    'inquiry'=>array(
                        'type'=>Literal::class,
                        'options'=>array(
                            'route'=>'/inquiry',
                            'defaults'=>array(
                                'controller'=>'AdminModule\Controller\AngularListForm',
                                'action'=>'index'
                            )
                        ),
                        'may_terminate'=>true,
                        'child_routes'=>array(
                            'export'=>array(
                                'type'=>'Literal',
                                'options'=>array(
                                    'route'=>'/export',
                                    'defaults'=>array(
                                        'controller'=>ExportController::class,
                                        'action'=>'inquiryList',
                                    )
                                )
                            )
                        )
                    ),
                    'contact'=>array(
                        'type'=>Literal::class,
                        'options'=>array(
                            'route'=>'/contact',
                            'defaults'=>array(
                                'controller'=>'AdminModule\Controller\AngularListForm',
                                'action'=>'index'
                            )
                        ),
                        'may_terminate'=>true,
                        'child_routes'=>array(
                            'export'=>array(
                                'type'=>'Literal',
                                'options'=>array(
                                    'route'=>'/export',
                                    'defaults'=>array(
                                        'controller'=>ExportController::class,
                                        'action'=>'contactList',
                                    )
                                )
                            )
                        )
                    ),
                    'e-waste'=>array(
                        'type'=>Literal::class,
                        'options'=>array(
                            'route'=>'/e-waste',
                            'defaults'=>array(
                                'controller'=>'AdminModule\Controller\AngularListForm',
                                'action'=>'index'
                            )
                        ),
                        'may_terminate' => true,
                        'child_routes'=>array(
                            'export'=>array(
                                'type'=>'Literal',
                                'options'=>array(
                                    'route'=>'/export',
                                    'defaults'=>array(
                                        'controller'=>ExportController::class,
                                        'action'=>'ewaste',
                                    )
                                )
                            )
                        )
                    ),
                ),
            ),
        ),
    ),
    'controllers' => array(
        'factories' => array(
            ExportController::class=>ControllerFactory::class
        ),
    ),
);

