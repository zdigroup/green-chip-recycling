/**
 * Administrators Controllers
 */
angular.module('admin_module.controllers')

/**
 * List Controller
 */
    .controller('ContactListController', ['$scope', '$controller', '$location', function($scope, $controller, $location){
        "use strict";

        /**
         * Extend the Abstract List Controller
         */
        angular.extend(this, $controller('AbstractListController', {$scope: $scope}));


        /**
         * Export
         */
        $scope.export = function()
        {
            window.location = '/adx/contact/export';
        }

    }])

    /**
     * Form Controller
     */
    .controller('ContactFormController', ['$scope', '$controller', '$routeParams',
        function($scope, $controller, $routeParams) {
            "use strict";



            /**
             * Extend the Abstract List Controller
             */
            angular.extend(this, $controller('AbstractFormController', {$scope: $scope}));



        }])

    /**
     * Configuration
     */
    .config(['$routeProvider', '$provide', '$sceDelegateProvider', function($routeProvider, $provide, $sceDelegateProvider) {

        /**
         * The url part to test and register routes
         * @type {string}
         */
        var path = '/adx/contact';

        /**
         * The name of the List Controller
         * @type {string}
         */
        var listController = 'ContactListController';

        /**
         * The name of the Form Controller
         * @type {string}
         */
        var formController= 'ContactFormController';


        /**
         * The directory name holding the templates
         * @type {string}
         */
        var templateDir = 'admin/contact';

        /**
         * Register Configuration
         */
        if( window.location.pathname.indexOf(path) >= 0 && window.location.pathname.indexOf(path+'/') === -1)
        {
            angular.injector(['ng']).get('$log').log('Configuring for ' + path);


            /**
             * The List Form Configuration
             */
            $provide.factory("listFormConfiguration", function () {
                return {
                    dataKey:'contact',
                    apiUrl:'/api/forms/contact',
                    storageNamespace:'ContactListController',
                    page:1,
                    sortField:'datetime_created',
                    sortDirection:-1,
                    search:'',
                    deleteable:true
                };
            });

            $routeProvider.otherwise({
                templateUrl:'/templates/' + templateDir + '/list.html',
                controller:listController
            });

            $routeProvider.when('/add', {
                templateUrl:'/templates/' + templateDir + '/form.html',
                controller:formController,
                /*resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
                    loadNgTags: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('/vendor/ng-tags-input/ng-tags-input.min.js');
                    }]
                }*/
            });

            $routeProvider.when('/:id', {
                templateUrl:'/templates/' + templateDir + '/form.html',
                controller:formController,
                /*resolve: { // Any property in resolve should return a promise and is executed before the view is loaded
                    loadNgTags: ['$ocLazyLoad', function($ocLazyLoad) {
                        return $ocLazyLoad.load('/vendor/ng-tags-input/ng-tags-input.min.js');
                    }]
                }*/
            });
        }
    }])
