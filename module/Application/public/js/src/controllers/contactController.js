angular.module(_moduleNamespace).controller('contactController', ['apiService', '$scope', 'CATEGORIES', 'LOCATIONS', 'LOCATIONS_MUNICIPALITY',
    function(apiService, $scope, CATEGORIES, LOCATIONS, LOCATIONS_MUNICIPALITY) {

    $scope.form = {
        form: null
    };

    $scope.model = {

    };

    $scope.categories = CATEGORIES;

    $scope.$watch('model.category', function(newVal, oldVal) {

        // store current locations array in variable
        var currentLocations = $scope.locations;

        // only show locations choices if category is selected
        if(typeof newVal === 'undefined') {
            $scope.locations = {};
            return;
        }

        // change locations list when category is selected if needed
        if(newVal === 'Municipality Recycling Programs') {
            $scope.locations = LOCATIONS_MUNICIPALITY;
        } else {
            $scope.locations = LOCATIONS;
        }

        // clear location selection if needed
        if(currentLocations !== $scope.locations) {
            $scope.model.location = null;
        }

    });

    $scope.submitForm = function () {

        if($scope.form.form.$invalid) {
            return false;
        }

        $scope.processing = true;
        $scope.apiFormErrors = null;

        apiService.call({url: '/api/forms/contact', method: 'post', data: $scope.model }).then(
            function (response) {
                $scope.processed = true;
            },
            function (error) {
                $scope.apiFormErrors = error.data;
            }
        ).finally(function () {
            $scope.processing = false;
        });

    };
    

}]);
