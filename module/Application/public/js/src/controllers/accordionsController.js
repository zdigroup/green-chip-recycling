angular.module(_moduleNamespace).controller('accordionsController', ['$scope', function($scope) {

    $scope.accordions = {
        active: ''
    };

    $scope.selectAccordion = function(accordion) {
        if($scope.accordions.active === accordion) {
            $scope.accordions.active = '';
        } else {
            $scope.accordions.active = accordion;
        }
    };


}]);
