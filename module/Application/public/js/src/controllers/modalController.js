angular.module(_moduleNamespace).controller('modalController', ['$scope', 'ngDialog',
    function($scope, ngDialog) {



        ngDialog.open({
            template: '/templates/application/warning-modal.html',
            className: 'ngdialog-theme-default',
            closeByDocument: true,
            showClose: false,
            scope:$scope,
            cache: false
        });


    }]);
