angular.module(_moduleNamespace).controller('panelController', ['apiService', '$scope', 'CATEGORIES', 'LOCATIONS', 'LOCATIONS_MUNICIPALITY',
    function(apiService, $scope, CATEGORIES, LOCATIONS, LOCATIONS_MUNICIPALITY) {

    $scope.form = {
        form: null,
        mobile: null
    };

    $scope.model = {};

    $scope.panel = {
        opened: true
    };

    $scope.categories = CATEGORIES;

    $scope.closeModal = function() {
        $scope.panel.opened = false;
    };

    $scope.$watch('model.category', function(newVal, oldVal) {

        // store current locations array in variable
        var currentLocations = $scope.locations;

        // only show locations choices if category is selected
        if(typeof newVal === 'undefined') {
            $scope.locations = {};
            return;
        }

        // change locations list when category is selected if needed
        if(newVal === 'Municipality Recycling Programs') {
            $scope.locations = LOCATIONS_MUNICIPALITY;
        } else {
            $scope.locations = LOCATIONS;
        }

        // clear location selection if needed
        if(currentLocations !== $scope.locations) {
            $scope.model.location = null;
        }

    });

    $scope.submitForm = function (form) {

        if(form === 'form') {
            if($scope.form.form.$invalid) {
                return false;
            }
        }

        if(form === 'mobile') {
            if($scope.form.mobile.$invalid) {
                return false;
            }
        }


        $scope.processing = true;
        $scope.apiFormErrors = null;

        apiService.call({url: '/api/forms/inquiry', method: 'post', data: $scope.model }).then(
            function (response) {
                $scope.processed = true;
            },
            function (error) {
                $scope.apiFormErrors = error.data;
            }
        ).finally(function () {
            $scope.processing = false;
        });

    };


}]);
