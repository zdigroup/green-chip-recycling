angular.module(_moduleNamespace).controller('eWasteFormController', ['$scope', '$timeout', '$q', 'apiService', function($scope, $timeout, $q, apiService) {


    $scope.formStep = 0;
    $scope.maxStepReached = 0;

    $scope.uploadFile = {
        file: null
    };

    $scope.form = {
        'form': null
    };

    // hacky flags that have to be used for google address autocomplete
    $scope.showWhereLocated = false;
    $scope.showElevator = false;
    $scope.showLoadingDock = false;

    var autocomplete;

    function initAutocomplete() {
        var input = document.getElementById("autocompleteAddress");
        console.log('input', input);
        autocomplete = new google.maps.places.Autocomplete(input);
        console.log('autocomplete', autocomplete);
        autocomplete.addListener('place_changed', fillInAddress);
    }

    function fillInAddress() {
        var address = autocomplete.getPlace();
        console.log('address', address);
        $scope.model.requiredServices.common.model.whereLocated = address.formatted_address;
        $scope.$apply();
        console.log($scope.model.requiredServices.common.model.whereLocated);
    }

    google.maps.event.addDomListener(window, 'load', initAutocomplete);



    $scope.eWasteOptions = [
        { id: 'PC Tower', label: 'PC Tower' },
        { id: 'Laptop', label: 'Laptop' },
        { id: 'Cell Phone', label: 'Cell Phone' },
        { id: 'Server', label: 'Server' },
        { id: 'Copier', label: 'Copier' },
        { id: 'Fax', label: 'Fax' },
        { id: 'Flat Monitor', label: 'Flat Monitor' },
        { id: 'CRT Monitor', label: 'CRT Monitor' },
        { id: 'Television', label: 'Television' },
        { id: 'Network Equipment', label: 'Network Equipment' },
        { id: 'Printer, over 100lb', label: 'Printer, over 100lb' },
        { id: 'Printer, under 100lb', label: 'Printer, under 100lb' },
        { id: 'Power Supply', label: 'Power Supply' },
        { id: 'Hard Drive', label: 'Hard Drive' },
        { id: 'PC Cable', label: 'PC Cable' },
        { id: 'Keyboard / Mouse', label: 'Keyboard / Mouse' },
        { id: 'Floppy / CD Drive', label: 'Floppy / CD Drive' },
        { id: 'Other', label: 'Other' }
    ];

    $scope.hardDrivesOptions = [
        { id: 'inCPU', 'label': "No, the hard drives are still inside the devices."},
        { id: 'loose', 'label': "Yes, the hard drives are loose."},
    ];

    $scope.recycleComputerOptions = [
        { id: 'recycle', 'label': "Yes, recycle the devices."},
        { id: 'dontRecycle', 'label': "No, don't recycle the devices."},
    ];

    $scope.onSiteShreddingOptions = [
        { id: 'onSite', 'label': "Yes, I would like shredding on-site."},
        { id: 'offSite', 'label': "No, I would not like shredding on-site."},
    ];

    $scope.requiredServicesOptions = [
        { id: 'ewaste', label: 'E-Waste Removal' },
        { id: 'hardDrive', label: 'Hard Drive Shredding' },
        { id: 'itad', label: 'ITAD Services' },
    ];

    $scope.assetListOptions = [
        { id: "Yes", label: "Yes, I have a list of equipment." },
        { id: "No", label: "No, I don't have a list of equipment." },
        // { id: 'skip', label: "Skip this step."},
    ];

    $scope.auditableInvOptions = [
        { id: "Yes", label: "Yes, I need an auditable inventory report." },
        { id: "No", label: "No, I don't need an auditable inventory report." }
    ];

    $scope.loadingDockOptions = [
        { id: "Yes", label: "Yes, there is a loading dock." },
        { id: "No", label: "No, there is not a loading dock." }
    ];

    $scope.elevatorAccessOptions = [
        { id: "Yes", label: "Yes, there is freight or passenger elevator access." },
        { id: "No", label: "No, there is not elevator access." }
    ];

    $scope.stairsInPathOptions = [
        { id: "Yes", label: "Yes, there are stairs in the path of removal." },
        { id: "No", label: "No, there aren't stairs in the path of removal." }
    ];

    function initModel() {
        $scope.model = {
            uploadedFiles: [],
            requiredServices: {
                ewaste: {
                    selected: false,
                    model: {
                        providedList: [
                            { item: '', quantity: '', other: ''}
                        ]
                    }
                },
                hardDrive: { selected: false, model: {} },
                itad: { selected: false, model: {} },
                common: {
                    model: {

                    }
                }
            }
        };
    }

    initModel();

    $scope.dropdowns = {};

    $scope.checkAssetList = function() {
        if($scope.model.requiredServices.ewaste.model.haveAssetList === 'Yes' ||
           $scope.model.requiredServices.itad.model.haveAssetList === 'Yes' ||
           $scope.model.requiredServices.hardDrive.model.haveAssetList === 'Yes') {
            return true;
        }
        return false;
    };

    $scope.$watch('model.requiredServices', function(newVal, oldVal) {
        if(newVal !== oldVal) {
            $scope.detectRequirements();
        }
    }, true);

    $scope.$watch('dropdowns.requiredServices', function(newVal, oldVal) {
        $scope.model.requiredServices.ewaste.selected = false;
        $scope.model.requiredServices.hardDrive.selected = false;
        $scope.model.requiredServices.itad.selected = false;
        if(newVal && newVal.length) {
            for(var i = 0; i < newVal.length; i++) {
                var category = newVal[i];
                $scope.model.requiredServices[category].selected = true;
            }
            $scope.formStep = 0;
        }
    }, true);

    var components = {
        commonUserInformation: [
            {id: 'contactInformation', model: 'common'}
        ],
        ewasteComponents: [
            {id: 'haveAssetList'},
            {id: 'providedList', condition: {field: 'haveAssetList', value: "No"}},
            {id: 'needAuditableInventory'}
        ],
        hardDriveComponents: [
            {id: 'howManyDrives'},
            {id: 'looseOrInCPU'},
            {id: 'shredOnsite', condition: {field: 'looseOrInCPU', value: 'loose'}},
            {id: 'additionalShredding', condition: {field: 'shredOnSite', value: true}},
            {id: 'recycleWithDrivesInside', condition: {field: 'looseOrInCPU', value: 'inCPU'}},
            {id: 'providedList', condition: {field: 'recycleWithDrivesInside', value: "Yes"}},
            {id: 'additionalShredding'},
        ],
        itadComponents: [
            {id: 'haveAssetList'},
         //   {id: 'uploadList', condition: {field: 'haveAssetList', value: "Yes"}},
        ],
        commonComponents: [
            {id: 'whereLocated', model: 'common'},
            {id: 'loadingDockElevator', model: 'common'},
            {id: 'elevatorOrStairs', model: 'common', condition: {field: 'loadingDockElevator', value: "No"}},
        ]
    }

    $scope.requiredFormComponents = [];


    $scope.detectRequirements = function() {

        var workingArray = [];

        // common components
        for (var i = 0; i < components.commonUserInformation.length; i++) {
            var comp = components.commonUserInformation[i];
            workingArray.push(comp);
        }

       // console.group('detectRequirements');
        for (var prop in $scope.model.requiredServices) {
          //  console.log('Working with model prop', prop, $scope.model.requiredServices[prop].selected);
            if ($scope.model.requiredServices[prop].selected) {
                var componentKey = prop + 'Components';
          //      console.log('Working with', prop, componentKey);

                for (var a = 0; a < components[componentKey].length; a++) {
                    var cConf = JSON.parse(JSON.stringify(components[componentKey][a]));
                    cConf.model = prop;

                    if (typeof cConf.condition === 'undefined' || ! cConf.condition) {
                    //    console.log('No Condition, pushing', componentKey, cConf);
                        workingArray.push(cConf);
                    } else {
                   //     console.log('Testing Condition', componentKey, cConf);
                        if ($scope.model.requiredServices[prop].model[cConf.condition.field] === cConf.condition.value) {
                            workingArray.push(cConf);
                        }
                    }
                }
            }
        }

        // common components
        for (var i = 0; i < components.commonComponents.length; i++) {
            var comp = components.commonComponents[i];

            if(!comp.condition) {
                workingArray.push(comp);
            } else {
                if($scope.model.requiredServices.common.model[comp.condition.field] === comp.condition.value) {
                    workingArray.push(comp);
                }
            }
        }

        //console.log('Configured Working Components', JSON.parse(JSON.stringify(workingArray)));
        $scope.requiredFormComponents.length = 0;

        for (var i = 0; i < workingArray.length; i++) {
            $scope.requiredFormComponents.push(workingArray[i]);
          //  console.log('Configured Working Components Array Finalized', $scope.requiredFormComponents);
        }

       // console.groupEnd();

        console.log('finished generating required form components');

    };

    $scope.toggleService = function(id) {
        $scope.model.requiredServices[id].selected = !$scope.model.requiredServices[id].selected;
    }

    $scope.nextStep = function() {

        $scope.showWhereLocated = false;
        $scope.showElevator = false;
        $scope.showLoadingDock = false;

        if($scope.requiredFormComponents[$scope.formStep].id === 'whereLocated') {
            $scope.showWhereLocated = true;
        }

        if($scope.requiredFormComponents[$scope.formStep].id === 'loadingDockElevator') {
            $scope.showLoadingDock = true;
        }

        if($scope.requiredFormComponents[$scope.formStep].id === 'elevatorOrStairs') {
            $scope.showElevator = true;
        }

        if($scope.formStep !== $scope.requiredFormComponents.length) {

            if($scope.formStep === 1) {
                $scope.form.form.$submitted = true;
                if($scope.form.form.$invalid) {
                    return false;
                }
            }

            $scope.form.form.$submitted = false;
            $scope.formStep++;

        }

        if($scope.maxStepReached < $scope.formStep) {
            $scope.maxStepReached = $scope.formStep;
        }


    }

    $scope.prevStep = function () {
        if($scope.formStep !== 0) {
            $scope.formStep--;
        }
    }

    $scope.reportModal = false;

    $scope.openFileUpload = function(id) {
        angular.element(id).click();
    }


    $scope.removeFile = function(index) {
        swal({
            title: "Are you sure?",
            text: "Are you sure you want to remove this file?",
            type: "warning",
            showCancelButton: true,
            confirmButtonText: "Delete",
            closeOnConfirm: true,
            html: false
        }, function() {
            $scope.model.uploadedFiles.splice(index, 1);
        });
    }

    $scope.$watch('uploadFile.file', function(newVal, oldVal) {
        if( ! Object.isEmpty(newVal) && newVal !== oldVal) {
            console.log('FILE', $scope.uploadFile.file, $scope.uploadFile.file.name);
            $scope.model.uploadedFiles.push(newVal);
            $scope.uploadFile.file = null;
        }
    });

    $scope.openReportModal = function() {
        $scope.reportModal = true;
    }

    $scope.closeReportModal = function() {
        $scope.reportModal = false;
    }

    /**
     * Start the submit sequence
     */
    $scope.submitForm = function() {

        if ($scope.processing) {
            return; // already processing!
        }

        if ($scope.form.form.$invalid) {
            return;
        }

        $scope.processing = true;
        $scope.error = null;

        if($scope.model.uploadedFiles.length) {
            $scope.sendFiles().then(
                function(response) {
                    $scope.model.uploadedFiles = response;
                    $scope.sendForm().then(
                        function(response) {
                            $scope.processed = true;
                        },
                        function(error) {
                            $scope.error = error;
                        }).finally(function() {
                        $scope.processing = false;
                    });
                },
                function(error) {
                    $scope.error = error;
                    $scope.processing = false;
                });
        } else {
            $scope.sendForm().then(
                function(response) {
                    $scope.processed = true;
                },
                function(error) {
                    $scope.error = error;
                }).finally(function() {
                    $scope.processing = false;
                })
;        }

    }

    /**
     * Send any files if necessary first
     * @returns {*}
     */
    $scope.sendFiles = function() {
        var promise = $q.defer();

        if ($scope.model.uploadedFiles.length) {
            var headers = {
                'Content-Type': undefined,
                'Accept': 'application/json'
            };

            var formData = new FormData();

            for (var i = 0; i < $scope.model.uploadedFiles.length; i++) {
                formData.append('files['+i+']', $scope.model.uploadedFiles[i]);
            }

            apiService.call({url: '/api/forms/upload-files', method: 'POST', data: formData, headers: headers}).then(
                function(response) {
                    promise.resolve(response.data.files);
                },
                function(error) {
                    promise.reject(error);
                });

        } else {
            promise.resolve(null);
        }

        return promise.promise;
    }

    /**
     * Send the form data
     * @returns {*}
     */
    $scope.sendForm = function() {
        var promise = $q.defer();

        apiService.call({url: '/api/forms/waste', method: 'POST', data: $scope.model}).then(
            function(response) {
                promise.resolve(response.data);
                $scope.processed = true;
            },
            function(error) {
                promise.reject(error);
            });

        return promise.promise;
    }

    $scope.resetForm = function() {
        initModel();
        $scope.processed = false;
        $scope.formStep = 0;
        $scope.dropdowns.requiredServices = null;
    }

    $scope.openExplanation = function() {
        swal({
            title: "<span class='block custom-title'>On-Site vs. Off-Site</span>",
            text:
                '<span class="block spaced">With on-site shredding service, each drive will be completely destroyed on your company premises. The service will be accompanied by a Certificate of Destruction and an auditable hard drive destruction inventory, including serial number of each drive.</span>' +
                '<span class="block">With off-site shredding we will securely remove the hard drives from your premises in locking totes and transport them with GPS tracked trucking to the secure Greenchip facility where the drives will be completely destroyed. The service will be accompanied by a Certificate of Destruction and an auditable hard drive destruction inventory, including serial number of each drive.</span>',
            type: "info",
            showCancelButton: false,
            confirmButtonText: "OK",
            closeOnConfirm: true,
            html: true,
            className: "custom"
        });
    }

    $scope.getArray = function(length) {
        return new Array(length);
    };

    $scope.addAnotherEwasteType = function() {
        $scope.model.requiredServices.ewaste.model.providedList.push({
            item: '',
            quantity: '',
            other: ''
        });
    }

    $scope.setFormStep = function(index) {
        if(index <= $scope.maxStepReached) {
            $scope.formStep = index;
        }
    }



}]);
