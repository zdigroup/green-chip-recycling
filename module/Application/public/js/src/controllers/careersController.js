angular.module(_moduleNamespace).controller('careersController', ['$scope', function($scope) {

    $scope.jobs = {
        operations: false,
        customer: false
    }

    $scope.toggleJob = function(job) {
        $scope.jobs[job] = !$scope.jobs[job];
    }


}]);
