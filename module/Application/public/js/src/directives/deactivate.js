angular.module(_moduleNamespace).directive('deactivate', [function () {

    return {
        restrict: 'A',
        controller: [
            '$scope',
            '$element',
            '$attrs',
            function ($scope, $element, $attrs) {
                $($element).click(function () {
                    $($attrs.deactivate).removeClass('active');
                });
            }
        ]
    };

}]);