angular.module(_moduleNamespace).directive('toggleActive', [function () {

    return {
        restrict: 'A',
        controller: [
            '$scope',
            '$element',
            '$attrs',
            function ($scope, $element, $attrs) {
                $($element).click(function () {
                    $($attrs.toggleActive).toggleClass('active');
                });
            }
        ]
    };

}]);