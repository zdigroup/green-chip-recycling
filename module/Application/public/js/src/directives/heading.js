angular.module(_moduleNamespace).directive('heading', function() {

    return {
        restrict: 'E',
        template:
        '<div class="row center border-row">' +
        '<div class="col-free border-left"></div>' +
        '<div class="col-free border-center">' +
        '<h2>{{ text }}</h2>' +
        '</div>' +
        '<div class="col-free border-right"></div>'
        ,
        scope: {
            text: '@'
        },
        link: function(scope) {

        }
    }

});