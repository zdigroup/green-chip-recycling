/**
 * ZDI Design Group
 * Project east-wind
 * Author derekmiranda
 * Date: 12/23/15 2:10 PM
 *
 * (c) Copyright 2015 derekmiranda | All Rights Reserved
 */
angular.module(_moduleNamespace).directive('bindFile', [function () {
    return {
        require: "ngModel",
        restrict: 'A',
        link: function (scope, el, attrs, ngModel) {

            /**
             * Init destructors
             * @type {Array}
             */
            var destructors = [];

            function onChange(event) {
                ngModel.$setViewValue(event.target.files[0]);
                scope.$apply();
            }

            el.bind('change', onChange);

            destructors.push(scope.$watch(function () {
                return ngModel.$viewValue;
            }, function (value) {
                if (!value) {
                    el.val("");
                }
            }));

            /**
             * On Destroy
             */
            scope.$on('$destroy', function() {
                for(var i = 0; i < destructors.length; i++ ) {
                    destructors[i]();
                }

                if (typeof el !== 'undefined' && el !== null && typeof el.removeEventListener !== 'undefined' && el.removeEventListener !== null) {
                    el.removeEventListener('change', onChange);
                }
            });
        }
    };
}]);
