angular.module(_moduleNamespace).directive('slideTo', [function () {

    return {
        restrict: 'A',
        scope: {
            preventSlide: '='
        },
        controller: [
            '$scope',
            '$element',
            '$attrs',
            function ($scope, $element, $attrs) {

                $($element).click(function (e) {

                    if($scope.preventSlide) {
                        return;
                    }

                    var slideTo = $attrs.slideTo;
                    var speed = $attrs.speed || 500;

                    var offset = $($attrs.offset).height() || 0;
                    var offsetValue = parseInt($attrs.offsetValue) || 0;

                    $('html, body').animate({
                        scrollTop: $(slideTo).offset().top - (offset+offsetValue)
                    }, speed);

                });

            }
        ]
    };

}]);
