angular.module(_moduleNamespace).directive('animateIn', [function () {

    return {
        restrict: 'A',
        controller: [
            '$scope',
            '$element',
            '$attrs',
            function ($scope, $element, $attrs) {
                function animateIn() {

                    if($($element).hasClass('animate-in')) {
                        return;
                    }

                    var elementOffset = $($element).offset().top;
                    var scroll = $(document).scrollTop();
                    var multiplier = $attrs.windowMultiplier || .5;
                    var windowWidth = $(window).width();

                    if($attrs.mobileWindowMultiplier && windowWidth <= 767) {
                        multiplier = $attrs.mobileWindowMultiplier;
                    }

                    var windowHeight = ($(window).height() * multiplier);

                    if ((windowHeight + scroll) > elementOffset) {
                        $($element).addClass('animate-in');
                    }

                }

                animateIn();
                $(window).scroll(animateIn);

            }
        ]
    };

}]);