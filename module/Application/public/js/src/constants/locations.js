angular.module(_moduleNamespace).constant('LOCATIONS', [
    {
        id: 'Business',
        label: 'Business'
    },
    {
        id: 'Non-Profit',
        label: 'Non-Profit'
    },
    {
        id: 'Government Agency',
        label: 'Government Agency'
    },
    {
        id: 'Healthcare Institution',
        label: 'Healthcare Institution'
    },
    {
        id: 'School',
        label: 'School'
    }
]);

angular.module(_moduleNamespace).constant('LOCATIONS_MUNICIPALITY', [
    {
        id: 'County',
        label: 'County'
    },
    {
        id: 'Town',
        label: 'Town'
    },
    {
        id: 'City',
        label: 'City'
    }
]);