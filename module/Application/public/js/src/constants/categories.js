angular.module(_moduleNamespace).constant('CATEGORIES', [
    {
        id: 'IT Asset Disposition',
        label: 'IT Asset Disposition',
    },
    {
        id: 'Electronics Recycling',
        label: 'Electronics Recycling'
    },
    {
        id: 'Hard Drive Shredding',
        label: 'Hard Drive Shredding'
    },
    {
        id: 'Municipality Recycling Programs',
        label: 'Municipality Recycling Programs'
    },
    {
        id: 'CRT Glass',
        label: 'CRT Glass'
    },
    {
        id: 'Data Center Management',
        label: 'Data Center Management'
    }
]);
