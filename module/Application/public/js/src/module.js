/**
 * ZDI Design Group
 * Project zdi-design-v2
 * Author derekmiranda
 * Date: 12/28/15 4:21 PM
 *
 * (c) Copyright 2015 derekmiranda | All Rights Reserved
 */

const _moduleNamespace = 'gc';


angular.module(_moduleNamespace, [
    'api-validation-messages', 'ngMask', 'ngAnimate', 'ngDialog', 'ngStorage', 'bcherny/formatAsCurrency', 'angular-custom-dropdown'])
    .config(['$httpProvider', function ($httpProvider) {

}]);

