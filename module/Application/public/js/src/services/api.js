/**
 * ZDI Design Group
 * Project zdi-design-v2
 * Author derekmiranda
 * Date: 12/28/15 4:33 PM
 *
 * (c) Copyright 2015 derekmiranda | All Rights Reserved
 */
/**
 * ZDI Design Group
 * Project Kachoo
 * Author derekmiranda
 * Date: 9/2/15 2:56 PM
 *
 * (c) Copyright 2015 derekmiranda | All Rights Reserved
 */
angular.module(_moduleNamespace)
    .factory('apiService', ['$http', '$q', '$log', function ($http, $q, $log) {

        /**
         * Makes an API Call
         * @param params
         * @returns {promise.promise}
         */
        this.call = function (params) {
            $log.log('Api Call:', params);

            var promise = $q.defer();

            $http(params).then(
                function (response) {
                    $log.log('Api Response:', response);
                    return promise.resolve(response);
                },
                function (error) {
                    $log.error('Api Error', error);
                    return promise.reject(error);
                }
            );

            return promise.promise;
        }

        return this;
    }]);