$(document).ready(function() {

    function setHeroHeight() {
        var height = $(window).height();
        $('.home-hero').height(height);
    }

    setHeroHeight();
    $(window).resize(setHeroHeight);

});