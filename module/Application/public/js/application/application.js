$(document).ready(function() {


    var body = $('body');

    body.imagesLoaded(function() {
        setTimeout(function() {
            body.addClass('images-loaded');
        }, 250);
    });


    function scrolled() {
        var scrollTop = $(document).scrollTop();
        if(scrollTop > 0) {
            body.addClass('scrolled');
        } else {
            body.removeClass('scrolled');
        }
    }

    scrolled();
    $(window).scroll(scrolled);


    if ($('html').hasClass('modernizr-no-touchevents')) {
        skrollr.init({
            forceHeight: false
        });
    }


});
