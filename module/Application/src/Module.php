<?php
/**
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3-Clause
 * @copyright Copyright (c) 2014-2016 Zend Technologies USA Inc. (http://www.zend.com)
 */

namespace Application;

use Application\Factory\ServiceLocatorFactory;
use Zend\Http\PhpEnvironment\Response;
use Zend\Mvc\MvcEvent;

/**
 * Class Module
 * @package Application
 */
class Module
{
    /**
     * @return array
     */
    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }

    /**
     * @param MvcEvent $e
     */
    public function onBootstrap(MvcEvent $e)
    {
        $em = $e->getApplication()->getEventManager();
        $em->attach(MvcEvent::EVENT_DISPATCH_ERROR, array($this, 'redirect'));
    }

    /**
     * @param MvcEvent $e
     * @return Response
     */
    public function redirect(MvcEvent $e)
    {
        $uri = $e->getRequest()->getUri()->getPath();
        $plugInManager = $e->getApplication()->getServiceManager()->get('ViewHelperManager');
        $urlMaker = $plugInManager->get('url');
        $redirectTo = null;


        switch($uri) {
            case '/certifications.php':
            case '/environmental-policy.php':
                {
                    $redirectTo = $urlMaker('certifications');
                    break;
                }
            case '/corporate.php':
                {
                    $redirectTo = $urlMaker('it-asset-disposition');
                    break;
                }
            case '/e-waste.php':
            case '/ewaste-removal.php':
                {
                    $redirectTo = $urlMaker('electronics-recycling');
                    break;
                }
            case '/data-destruction.php':
            case '/mobile-hard-drive-shredding.php':
                {
                    $redirectTo = $urlMaker('hard-drive-shredding');
                    break;
                }
            case '/news.php':
            case '/company.php':
            case '/facility.php':
                {
                    $redirectTo = $urlMaker('about');
                    break;
                }
            case '/faqs.php':
            case '/why.php':
            case '/process.php':
            case '/programs.php':
            case '/problem.php':
            case '/nys.php':
            case '/federal.php':
            case '/awareness.php':
            case '/where.php':
            case '/documentation.php':
            case '/white-glove.php':
            case '/asset-recovery.php':
            case '/ewastelaw2.pdf':
                {
                    $redirectTo = $urlMaker('home');
                    break;
                }
            case '/contact.php':
            case '/ElectronicWasteRemovalForm.pdf':
            case '/ewaste-removal-form.php':
                {
                    $redirectTo = $urlMaker('contact');
                    break;
                }
        }

        if( $redirectTo !== null )
        {
            /**
             * @var $response Response
             */

            $response = $e->getResponse();
            $response->setStatusCode(301);
            $response->getHeaders()->addHeaderLine('Location', $redirectTo);
            return $response;
        }
    }
}
