<?php
/**
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3-Clause
 * @copyright Copyright (c) 2014-2016 Zend Technologies USA Inc. (http://www.zend.com)
 */

namespace Application\Controller;

use AdvancedActionController\Controller\AbstractActionController;

class CapabilityStatementController extends AbstractActionController
{
    public function indexAction()
    {
        return $this->redirect()->toUrl('/pdf/4.30.24_Green_Capabilitystatement_final.pdf');
    }
}
