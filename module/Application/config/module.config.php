<?php
/**
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3-Clause
 * @copyright Copyright (c) 2014-2016 Zend Technologies USA Inc. (http://www.zend.com)
 */

namespace Application;

use AdvancedActionController\Factory\ControllerFactory;
use AdvancedActionController\Router\Http\Literal;
use Application\ActiveCampaign\Factory;
use Application\Controller\AccessibilityController;
use Application\Controller\CareersController;
use Application\Controller\EhspolicyController;
use Application\Controller\EventsController;
use Application\Controller\EWasteFormController;
use Application\Controller\IndexController;
use Application\Controller\ContactController;
use Application\Controller\AboutController;
use Application\Controller\CertificationsController;
use Application\Controller\ITAssetDispositionController;
use Application\Controller\ElectronicsRecyclingController;
use Application\Controller\HardDriveShreddingController;
use Application\Controller\DataDestructionController;
use Application\Controller\NewsroomController;
use Application\Controller\IndustryNewsController;
use Application\Controller\GreenchipOnTheGoController;
use Application\Controller\CRTGlassController;
use Application\Controller\DataCenterSpecialistsController;
use Application\Controller\PrivacyPolicyController;
use Application\Controller\MunicipalityController;
use Application\Controller\OEMController;
use Application\Controller\TermsAndConditionsController;
use Application\Controller\CapabilityStatementController;

return [
    'router' => [
        'routes' => [
            'home' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/',
                    'defaults' => [
                        'controller' => IndexController::class,
                        'action'     => 'index',
                    ],
                ],
                'meta' => [
                    'title' => 'Greenchip | E-Waste & ITAD Solutions',
                    'meta' => [
                        'keywords' => 'ITAD, IT Asset Disposition, IT Asset Purchasing, NYC E-Waste, NYC E-Waste Recycling, Electronics Recycling, Recycling Office Equipment, R2, ISO14001, data destruction, NY Electronics Removal, E-Waste, E-Waste Recycling, E-Waste Facility, NYS Recycling Law, Hard Drive Shredding, NYS E-Waste Programs, Recycling New York, Electronic Waste Pick-Ups, E-Waste Disposal, E-Waste Compliance, E-Waste for businesses, E-Waste for medical offices, e-waste for financial institutions',
                        'description' => 'Greenchip is a leading provider of convenient, secure, reliable e-waste and ITAD services in New York City, the Tristate area, and beyond.',
                        'og:title' => 'Greenchip | E-Waste & ITAD Solutions',
                        'og:url' => APPLICATION_BASE_URL,
                        'og:type' => 'website',
                        'og:image' => APPLICATION_BASE_URL . '/img/common/facebook.jpg',
                        'og:description' => 'Greenchip is a leading provider of convenient, secure, reliable e-waste and ITAD services in New York City, the Tristate area, and beyond.'
                    ]
                ]
            ],
            'ehspolicy' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/ehspolicy',
                    'defaults' => [
                        'controller' => EhspolicyController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'e-waste-form' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/e-waste-form',
                    'defaults' => [
                        'controller' => EWasteFormController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'careers' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/careers',
                    'defaults' => [
                        'controller' => CareersController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'contact' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/contact',
                    'defaults' => [
                        'controller' => ContactController::class,
                        'action'     => 'index',
                    ],
                ],
                'meta' => [
                    'title' => 'Contact Greenchip | E-Waste & ITAD Solutions',
                    'meta' => [
                        'keywords'=>'ITAD, IT Asset Disposition, IT Asset Purchasing, NYC E-Waste, NYC E-Waste Recycling, Electronics Recycling, Recycling Office Equipment, R2, ISO14001, data destruction, NY Electronics Removal, E-Waste, E-Waste Recycling, E-Waste Facility, NYS Recycling Law, Hard Drive Shredding, NYS E-Waste Programs, Recycling New York, Electronic Waste Pick-Ups, E-Waste Disposal, E-Waste Compliance, E-Waste for businesses, E-Waste for medical offices, e-waste for financial institutions',
                        'description'=>'Contact Greenchip for fast, reliable E-Waste and ITAD Solutions.',
//                        'og:title'=>'',
//                        'og:url'=>'',
//                        'og:type'=>'',
//                        'og:image'=>'',
//                        'og:description'=>''
                    ]
                ]
            ],
            'about' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/about',
                    'defaults' => [
                        'controller' => AboutController::class,
                        'action'     => 'index',
                    ],
                ],
                'meta' => [
                    'title' => 'About Greenchip | E-Waste & ITAD Solutions',
                    'meta' => [
                        'keywords'=>'ITAD, IT Asset Disposition, IT Asset Purchasing, NYC E-Waste, NYC E-Waste Recycling, Electronics Recycling, Recycling Office Equipment, R2, ISO14001, data destruction, NY Electronics Removal, E-Waste, E-Waste Recycling, E-Waste Facility, NYS Recycling Law, Hard Drive Shredding, NYS E-Waste Programs, Recycling New York, Electronic Waste Pick-Ups, E-Waste Disposal, E-Waste Compliance, E-Waste for businesses, E-Waste for medical offices, e-waste for financial institutions',
                        'description'=>'Based in Brooklyn, New York and serving both the public and commercial sector, Greenchip Electronic Waste Solutions is one of the leading companies providing IT Asset Recovery and Electronics Recycling services.',
//                        'og:title'=>'',
//                        'og:url'=>'',
//                        'og:type'=>'',
//                        'og:image'=>'',
//                        'og:description'=>''
                    ]
                ]
            ],
            'oem' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/oem-program-management',
                    'defaults' => [
                        'controller' => OEMController::class,
                        'action'     => 'index',
                    ],
                ],
                'meta' => [
                    'title' => 'OEM Program Management | E-Waste & ITAD Solutions',
                    'meta' => [
                        //                     'keywords'=>'ITAD, IT Asset Disposition, IT Asset Purchasing, NYC E-Waste, NYC E-Waste Recycling, Electronics Recycling, Recycling Office Equipment, R2, ISO14001, data destruction, NY Electronics Removal, E-Waste, E-Waste Recycling, E-Waste Facility, NYS Recycling Law, Hard Drive Shredding, NYS E-Waste Programs, Recycling New York, Electronic Waste Pick-Ups, E-Waste Disposal, E-Waste Compliance, E-Waste for businesses, E-Waste for medical offices, e-waste for financial institutions',
                        //                      'description'=>'Based in Brooklyn, New York and serving both the public and commercial sector, Greenchip Electronic Waste Solutions is one of the leading companies providing IT Asset Recovery and Electronics Recycling services.',
//                        'og:title'=>'',
//                        'og:url'=>'',
//                        'og:type'=>'',
//                        'og:image'=>'',
//                        'og:description'=>''
                    ]
                ]
            ],
            'municipality' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/municipality-recycling-programs',
                    'defaults' => [
                        'controller' => MunicipalityController::class,
                        'action'     => 'index',
                    ],
                ],
                'meta' => [
                    'title' => 'Municipality Recycling Programs | E-Waste & ITAD Solutions',
                    'meta' => [
   //                     'keywords'=>'ITAD, IT Asset Disposition, IT Asset Purchasing, NYC E-Waste, NYC E-Waste Recycling, Electronics Recycling, Recycling Office Equipment, R2, ISO14001, data destruction, NY Electronics Removal, E-Waste, E-Waste Recycling, E-Waste Facility, NYS Recycling Law, Hard Drive Shredding, NYS E-Waste Programs, Recycling New York, Electronic Waste Pick-Ups, E-Waste Disposal, E-Waste Compliance, E-Waste for businesses, E-Waste for medical offices, e-waste for financial institutions',
  //                      'description'=>'Based in Brooklyn, New York and serving both the public and commercial sector, Greenchip Electronic Waste Solutions is one of the leading companies providing IT Asset Recovery and Electronics Recycling services.',
//                        'og:title'=>'',
//                        'og:url'=>'',
//                        'og:type'=>'',
//                        'og:image'=>'',
//                        'og:description'=>''
                    ]
                ]
            ],
            'certifications' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/certifications',
                    'defaults' => [
                        'controller' => CertificationsController::class,
                        'action'     => 'index',
                    ],
                ],
                'meta' => [
                    'title' => 'Greenchip | Industry Certifications | E-Waste & ITAD Solutions',
                    'meta' => [
                        'keywords'=>'ITAD, IT Asset Disposition, IT Asset Purchasing, NYC E-Waste, NYC E-Waste Recycling, Electronics Recycling, Recycling Office Equipment, R2, ISO14001, data destruction, NY Electronics Removal, E-Waste, E-Waste Recycling, E-Waste Facility, NYS Recycling Law, Hard Drive Shredding, NYS E-Waste Programs, Recycling New York, Electronic Waste Pick-Ups, E-Waste Disposal, E-Waste Compliance, E-Waste for businesses, E-Waste for medical offices, e-waste for financial institutions',
                        'description'=>'As a leader in managing e-waste, Greenchip Electronic Waste Solutions is fully R2 and ISO14001 certified, with annual auditing to maintain up-to-date certification. ',
//                        'og:title'=>'',
//                        'og:url'=>'',
//                        'og:type'=>'',
//                        'og:image'=>'',
//                        'og:description'=>''
                    ]
                ]
            ],
            'data-center-specialists' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/data-center-specialists',
                    'defaults' => [
                        'controller' => ITAssetDispositionController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'crt-glass' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/crt-glass',
                    'defaults' => [
                        'controller' => CRTGlassController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'it-asset-disposition' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/it-asset-disposition',
                    'defaults' => [
                        'controller' => ITAssetDispositionController::class,
                        'action'     => 'index',
                    ],
                ],
                'meta' => [
                    'title' => 'ITAD Experts | Greenchip | E-Waste & ITAD Solutions',
                    'meta' => [
                        'keywords'=>'ITAD, IT Asset Disposition, IT Asset Purchasing, NYC E-Waste, NYC E-Waste Recycling, Electronics Recycling, Recycling Office Equipment, R2, ISO14001, data destruction, NY Electronics Removal, E-Waste, E-Waste Recycling, E-Waste Facility, NYS Recycling Law, Hard Drive Shredding, NYS E-Waste Programs, Recycling New York, Electronic Waste Pick-Ups, E-Waste Disposal, E-Waste Compliance, E-Waste for businesses, E-Waste for medical offices, e-waste for financial institutions',
                        'description'=>'Greenchip Recycling has a specialized focus on ITAD Solutions with our Closed-Loop System ensuring your data is safe and you receive the best return on investment.',
//                        'og:title'=>'',
//                        'og:url'=>'',
//                        'og:type'=>'',
//                        'og:image'=>'',
//                        'og:description'=>''
                    ]
                ]
            ],
            'hard-drive-shredding' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/hard-drive-shredding',
                    'defaults' => [
                        'controller' => HardDriveShreddingController::class,
                        'action'     => 'index',
                    ],
                ],
                'meta' => [
                    'title' => 'Secure Data Destruction & Hard Drive Shredding | Greenchip | E-Waste & ITAD Solutions',
                    'meta' => [
                        'keywords'=>'ITAD, IT Asset Disposition, IT Asset Purchasing, NYC E-Waste, NYC E-Waste Recycling, Electronics Recycling, Recycling Office Equipment, R2, ISO14001, data destruction, NY Electronics Removal, E-Waste, E-Waste Recycling, E-Waste Facility, NYS Recycling Law, Hard Drive Shredding, NYS E-Waste Programs, Recycling New York, Electronic Waste Pick-Ups, E-Waste Disposal, E-Waste Compliance, E-Waste for businesses, E-Waste for medical offices, e-waste for financial institutions',
                        'description'=>'Greenchip offers mobile, secure hard-drive shredding for a safe, compliant, and convenient way to destroy your company’s old hard drives and electronics. ',
//                        'og:title'=>'',
//                        'og:url'=>'',
//                        'og:type'=>'',
//                        'og:image'=>'',
//                        'og:description'=>''
                    ]
                ]
            ],
            'electronics-recycling' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/electronics-recycling',
                    'defaults' => [
                        'controller' => ElectronicsRecyclingController::class,
                        'action'     => 'index',
                    ],
                ],
                'meta' => [
                    'title' => 'Reliable Electronics Recycling | Greenchip | E-Waste & ITAD Solutions',
                    'meta' => [
                        'keywords'=>'ITAD, IT Asset Disposition, IT Asset Purchasing, NYC E-Waste, NYC E-Waste Recycling, Electronics Recycling, Recycling Office Equipment, R2, ISO14001, data destruction, NY Electronics Removal, E-Waste, E-Waste Recycling, E-Waste Facility, NYS Recycling Law, Hard Drive Shredding, NYS E-Waste Programs, Recycling New York, Electronic Waste Pick-Ups, E-Waste Disposal, E-Waste Compliance, E-Waste for businesses, E-Waste for medical offices, e-waste for financial institutions',
                        'description'=>'Trust Greenchip with your E-Waste compliance needs, ensuring no recyclable electronics end up in landfills.',
//                        'og:title'=>'',
//                        'og:url'=>'',
//                        'og:type'=>'',
//                        'og:image'=>'',
//                        'og:description'=>''
                    ]
                ]
            ],
            'data-destruction' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/data-destruction',
                    'defaults' => [
                        'controller' => DataDestructionController::class,
                        'action'     => 'index',
                    ],
                ],
                'meta' => [
                    'title' => 'Secure Data Destruction | Greenchip | E-Waste & ITAD Solutions',
                    'meta' => [
                        'keywords'=>'ITAD, IT Asset Disposition, IT Asset Purchasing, NYC E-Waste, NYC E-Waste Recycling, Electronics Recycling, Recycling Office Equipment, R2, ISO14001, data destruction, NY Electronics Removal, E-Waste, E-Waste Recycling, E-Waste Facility, NYS Recycling Law, Hard Drive Shredding, NYS E-Waste Programs, Recycling New York, Electronic Waste Pick-Ups, E-Waste Disposal, E-Waste Compliance, E-Waste for businesses, E-Waste for medical offices, e-waste for financial institutions',
                        'description'=>'Greenchip offers fully-certified, secure data destruction for a safe, compliant, and convenient way to destroy your company’s old hard drives and electronics.',
//                        'og:title'=>'',
//                        'og:url'=>'',
//                        'og:type'=>'',
//                        'og:image'=>'',
//                        'og:description'=>''
                    ]
                ]
            ],
            'privacy-policy' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/privacy-policy',
                    'defaults' => [
                        'controller' => PrivacyPolicyController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'events' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/events',
                    'defaults' => [
                        'controller' => EventsController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'newsroom' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/newsroom',
                    'defaults' => [
                        'controller' => NewsroomController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'industry-news' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/industry-news',
                    'defaults' => [
                        'controller' => IndustryNewsController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'greenchip-on-the-go' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/greenchip-on-the-go',
                    'defaults' => [
                        'controller' => GreenchipOnTheGoController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'accessibility' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/accessibility',
                    'defaults' => [
                        'controller' => AccessibilityController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'capabilitystatement' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/capabilitystatement',
                    'defaults' => [
                        'controller' => CapabilityStatementController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
        ],
    ],
    'controllers' => [
        'factories' => [
            IndexController::class => ControllerFactory::class,
            ContactController::class => ControllerFactory::class,
            AboutController::class => ControllerFactory::class,
            CertificationsController::class => ControllerFactory::class,
            ITAssetDispositionController::class => ControllerFactory::class,
            HardDriveShreddingController::class => ControllerFactory::class,
            ElectronicsRecyclingController::class => ControllerFactory::class,
            DataDestructionController::class => ControllerFactory::class,
            PrivacyPolicyController::class => ControllerFactory::class,
            MunicipalityController::class => ControllerFactory::class,
            OEMController::class => ControllerFactory::class,
            EventsController::class => ControllerFactory::class,
            AccessibilityController::class => ControllerFactory::class,
            CareersController::class => ControllerFactory::class,
            EhspolicyController::class => ControllerFactory::class,
            EWasteFormController::class => ControllerFactory::class,
            NewsroomController::class => ControllerFactory::class,
            IndustryNewsController::class => ControllerFactory::class,
            GreenchipOnTheGoController::class => ControllerFactory::class,
            CRTGlassController::class => ControllerFactory::class,
            DataCenterSpecialistsController::class => ControllerFactory::class,
            CapabilityStatementController::class => ControllerFactory::class,
        ],
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => [
            'layout/layout'                             => __DIR__ . '/../view/layout/layout.phtml',
            'application/layout/header'                 => __DIR__ . '/../view/layout/header.phtml',
            'application/layout/footer'                 => __DIR__ . '/../view/layout/footer.phtml',
            'application/index/index'                   => __DIR__ . '/../view/application/index/index.phtml',
            'error/404'                                 => __DIR__ . '/../view/error/404.phtml',
            'error/index'                               => __DIR__ . '/../view/error/index.phtml',
            'capabilitystatement'                       => __DIR__ . '/../view/error/index.phtml',
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],
    'service_manager'=>[
        'factories'  => [
            'ActiveCampaign'=>Factory::class
        ],
    ],
    /**
     * View Helpers
     */
    'view_helpers'=>array(
        'invokables'=>array(
        ),
    ),
];
