<?php


class ContactsCest
{
    public function _before(ApiTester $I)
    {
    }

    public function _after(ApiTester $I)
    {
    }

    // tests
    public function tryToContact(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->haveHttpHeader('Accept', 'application/json');

        $I->sendPOST('contact/form', array('fred'=>'fred'));
        $I->seeResponseCodeIs(422);
        $I->seeResponseIsJson();
        $I->seeResponseJsonMatchesJsonPath('validation_messages.email');
        $I->seeResponseJsonMatchesJsonPath('validation_messages.first_name');
        $I->seeResponseJsonMatchesJsonPath('validation_messages.last_name');
        $I->seeResponseJsonMatchesJsonPath('validation_messages.message');
        $I->seeResponseJsonMatchesJsonPath('validation_messages.subject');


        $I->sendPOST('contact/form', array('email'=>'fred'));
        $I->seeResponseCodeIs(422);
        $I->seeResponseIsJson();
        $I->seeResponseJsonMatchesJsonPath('validation_messages');
        $I->seeResponseContainsJson(array(
            'validation_messages'=>array(
                'email'=>array(
                    'emailAddressInvalidFormat'=>'The input is not a valid email address. Use the basic format local-part@hostname'
                )
            )
        ));

        $postData = array(
            'first_name'=>'Teddy',
            'last_name'=>'Testerbaum',
            'email'=>'test@zdidesign.com',
            'subject'=>'Unit Test Contact Form',
            'message'=>'A test message for the contact form'
        );

        $I->sendPOST('contact/form', $postData);
        $I->seeResponseCodeIs(201);
        $I->seeResponseIsJson();
    }
}
