<?php


class DonationSingleCest
{
    const URL = 'donate/single';

    public function _before(ApiTester $I)
    {
    }

    public function _after(ApiTester $I)
    {
    }

    // tests
    public function tryToDonate(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->haveHttpHeader('Accept', 'application/json');

        $I->sendPOST(self::URL, array('fred'=>'fred'));
        $I->seeResponseCodeIs(422);

        $I->sendPOST(self::URL, array('email'=>'fred'));
        $I->seeResponseCodeIs(422);
        $I->seeResponseIsJson();
        $I->seeResponseJsonMatchesJsonPath('validation_messages');
        $I->seeResponseJsonMatchesJsonPath('validation_messages.email');
        $I->seeResponseJsonMatchesJsonPath('validation_messages.first_name');
        $I->seeResponseJsonMatchesJsonPath('validation_messages.last_name');
        $I->seeResponseJsonMatchesJsonPath('validation_messages.address');
        $I->seeResponseJsonMatchesJsonPath('validation_messages.city');
        $I->seeResponseJsonMatchesJsonPath('validation_messages.state');
        $I->seeResponseJsonMatchesJsonPath('validation_messages.zip');
        $I->seeResponseJsonMatchesJsonPath('validation_messages.phone');
        $I->seeResponseJsonMatchesJsonPath('validation_messages.donation_amount');
        $I->seeResponseJsonMatchesJsonPath('validation_messages.card_number');
        $I->seeResponseJsonMatchesJsonPath('validation_messages.card_number_clear');
        $I->seeResponseJsonMatchesJsonPath('validation_messages.card_exp');
        $I->seeResponseJsonMatchesJsonPath('validation_messages.card_cvc');
        $I->seeResponseJsonMatchesJsonPath('validation_messages.occupation');
        $I->seeResponseJsonMatchesJsonPath('validation_messages.employer');

        $postData = array(
            'first_name'=>'Test',
            'last_name'=>'Customer',
            'email'=>'test@zdidesign.com',
            'phone'=>6315551212,
            'donation_amount'=>10.00,
            'card_number'=>'XXXXXXXXXXXX1234',
            'card_number_clear'=>'4111111111111111',
            'card_exp'=>'1/00',
            'card_cvc'=>'123',
            'address'=>'60 George St',
            'city'=>'Babylon',
            'state'=>'NY',
            'zip'=>11111,
            'occupation'=>'Some Occupation',
            'employer'=>'Some Employer',
        );

        $I->sendPOST(self::URL, $postData);
        $I->seeResponseCodeIs(500);
        $I->seeResponseIsJson();
        $I->seeResponseContains('Card has expired by year');


        $postData = array(
            'first_name'=>'Test',
            'last_name'=>'Customer',
            'email'=>'test@zdidesign.com',
            'phone'=>6315551212,
            'donation_amount'=>10.00,
            'card_number'=>'XXXXXXXXXXXX1234',
            'card_number_clear'=>'4111111111111111',
            'card_exp'=>(date('m')-1) .'/'.date('y'),
            'card_cvc'=>'123',
            'address'=>'60 George St',
            'city'=>'Babylon',
            'state'=>'NY',
            'zip'=>11111,
            'occupation'=>'Some Occupation',
            'employer'=>'Some Employer',
        );

        $I->sendPOST(self::URL, $postData);
        $I->seeResponseCodeIs(500);
        $I->seeResponseIsJson();
        $I->seeResponseContains('Card has expired by month');


        $postData = array(
            'first_name'=>'Test',
            'last_name'=>'Customer',
            'email'=>'test@zdidesign.com',
            'phone'=>6315551212,
            'donation_amount'=>10.00,
            'card_number'=>'XXXXXXXXXXXX1234',
            'card_number_clear'=>'411111111111111X',
            'card_exp'=>'1/'.(date('y')+2),
            'card_cvc'=>'123',
            'address'=>'60 George St',
            'city'=>'Babylon',
            'state'=>'NY',
            'zip'=>11111,
            'occupation'=>'Some Occupation',
            'employer'=>'Some Employer',
        );

        $I->sendPOST(self::URL, $postData);
        $I->seeResponseCodeIs(422);
        $I->seeResponseIsJson();
        $I->seeResponseJsonMatchesJsonPath('validation_messages');
        $I->seeResponseJsonMatchesJsonPath('validation_messages.card_number_clear');

        // test the min donation
        $postData = array(
            'first_name'=>'Test',
            'last_name'=>'Customer',
            'email'=>'test@zdidesign.com',
            'phone'=>6315551212,
            'donation_amount'=>2.99,
            'card_number'=>'XXXXXXXXXXXX1234',
            'card_number_clear'=>'4111111111111111',
            'card_exp'=>'1/'.(date('y')+2),
            'card_cvc'=>'123',
            'address'=>'60 George St',
            'city'=>'Babylon',
            'state'=>'NY',
            'zip'=>11111,
            'occupation'=>'Some Occupation',
            'employer'=>'Some Employer',
        );

        $I->sendPOST(self::URL, $postData);
        $I->seeResponseCodeIs(422);
        $I->seeResponseIsJson();
        $I->seeResponseJsonMatchesJsonPath('validation_messages');
        $I->seeResponseJsonMatchesJsonPath('validation_messages.donation_amount');

        // test the max donation
        $postData = array(
            'first_name'=>'Test',
            'last_name'=>'Customer',
            'email'=>'test@zdidesign.com',
            'phone'=>6315551212,
            'donation_amount'=>1500.01,
            'card_number'=>'XXXXXXXXXXXX1234',
            'card_number_clear'=>'4111111111111111',
            'card_exp'=>'1/'.(date('y')+2),
            'card_cvc'=>'123',
            'address'=>'60 George St',
            'city'=>'Babylon',
            'state'=>'NY',
            'zip'=>11111,
            'occupation'=>'Some Occupation',
            'employer'=>'Some Employer',
        );

        $I->sendPOST(self::URL, $postData);
        $I->seeResponseCodeIs(422);
        $I->seeResponseIsJson();
        $I->seeResponseJsonMatchesJsonPath('validation_messages');
        $I->seeResponseJsonMatchesJsonPath('validation_messages.donation_amount');

        $postData['card_number_clear'] = '4111111111111111';
        $postData['donation_amount'] = 1500.00;

        $I->sendPOST(self::URL, $postData);
        $I->seeResponseCodeIs(201);
        $I->seeResponseIsJson();
        $I->seeResponseJsonMatchesJsonPath('payment_type');
        $I->seeResponseContainsJson(array('payment_type'=>'single'));

        $postData['donation_amount'] = 3.00;

        $I->sendPOST(self::URL, $postData);
        $I->seeResponseCodeIs(201);
        $I->seeResponseIsJson();
        $I->seeResponseJsonMatchesJsonPath('payment_type');
        $I->seeResponseContainsJson(array('payment_type'=>'single'));
    }
}
