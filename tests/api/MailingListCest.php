<?php


class MailingListCest
{
    public function _before(ApiTester $I)
    {
    }

    public function _after(ApiTester $I)
    {
    }

    // tests
    public function tryToSubscribe(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->haveHttpHeader('Accept', 'application/json');

        $I->sendPOST('mailing-list/form', array('fred'=>'fred'));
        $I->seeResponseCodeIs(422);

        $I->sendPOST('mailing-list/form', array('email'=>'fred'));
        $I->seeResponseCodeIs(422);
        $I->seeResponseIsJson();
        $I->seeResponseJsonMatchesJsonPath('validation_messages');
        $I->seeResponseContainsJson(array(
            'validation_messages'=>array(
                'email'=>array(
                    'emailAddressInvalidFormat'=>'The input is not a valid email address. Use the basic format local-part@hostname'
                )
            )
        ));

        $I->sendPOST('mailing-list/form', array('email'=>'test@zdidesign.com'));
        $I->seeResponseCodeIs(201);
        $I->seeResponseIsJson();
    }
}
