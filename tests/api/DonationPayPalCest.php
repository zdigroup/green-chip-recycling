<?php


class DonationPayPalCest
{
    const URL = 'donate/pay-pal';

    public function _before(ApiTester $I)
    {
    }

    public function _after(ApiTester $I)
    {
    }

    // tests
    public function tryToRecordPayPalResults(ApiTester $I)
    {
        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->haveHttpHeader('Accept', 'application/json');

        $I->sendPOST(self::URL, array('fred'=>'fred'));
        $I->seeResponseCodeIs(422);

        $I->sendPOST(self::URL, array('email'=>'fred'));
        $I->seeResponseCodeIs(422);
        $I->seeResponseIsJson();
        $I->seeResponseJsonMatchesJsonPath('validation_messages');
        $I->seeResponseJsonMatchesJsonPath('validation_messages.email');
        $I->seeResponseJsonMatchesJsonPath('validation_messages.first_name');
        $I->seeResponseJsonMatchesJsonPath('validation_messages.last_name');
        $I->seeResponseJsonMatchesJsonPath('validation_messages.phone');
        $I->seeResponseJsonMatchesJsonPath('validation_messages.address');
        $I->seeResponseJsonMatchesJsonPath('validation_messages.city');
        $I->seeResponseJsonMatchesJsonPath('validation_messages.state');
        $I->seeResponseJsonMatchesJsonPath('validation_messages.zip');
        $I->seeResponseJsonMatchesJsonPath('validation_messages.donation_amount');
        $I->seeResponseJsonMatchesJsonPath('validation_messages.occupation');
        $I->seeResponseJsonMatchesJsonPath('validation_messages.employer');

        $postData = array(
            'first_name'=>'Test',
            'last_name'=>'Customer',
            'email'=>'test@zdidesign.com',
            'phone'=>6315551212,
            'donation_amount'=>10.00,
            'address'=>'60 George St',
            'city'=>'Babylon',
            'state'=>'NY',
            'zip'=>11111,
            'occupation'=>'Some Occupation',
            'employer'=>'Some Employer'
        );

        $I->sendPOST(self::URL, $postData);
        $I->seeResponseCodeIs(201);
        $I->seeResponseIsJson();
        $I->seeResponseJsonMatchesJsonPath('payment_type');
        $I->seeResponseContainsJson(array('payment_type'=>'paypal'));

    }
}
