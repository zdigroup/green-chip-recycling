<?php

require_once(__DIR__ . '/traits/CommonSiteTestsTrait.php');


class HomePageCest
{

    use CommonSiteTestsTrait;

    public function _before(AcceptanceTester $I)
    {
        $I->amOnPage('/');
//        $I->click('.close-modal');
//        $I->wait(2);
    }

    public function _after(AcceptanceTester $I)
    {
    }

    public function testPageLoad(AcceptanceTester $I)
    {
        $I->wantToTest('that the page content loads');
        $I->canSeeElement('.hero');
        $I->cantSeeElement('.application-error');
    }

    public function testHeroAnimation(AcceptanceTester $I)
    {
        $I->wantToTest('that the hero content animates in');
        $I->wait(2);
        $checkForBodyClass = $I->executeJS('return $("body").hasClass("images-loaded")');
        if(!$checkForBodyClass)
        {
            throw new \Codeception\Exception\Fail('the class needed for hero animations is not being added to the body element');
        }
        $I->canSeeElement('.hero h1');
        $I->canSeeElement('.hero h2');
        $I->canSeeElement('.form-panel');
    }

    public function testTopSectionAnimation(AcceptanceTester $I)
    {
        $I->wantToTest('that the top section elements animate in');
        $I->scrollTo('footer');
        $I->wait(2);
        $I->canSeeElement('.top p');
        $I->canSeeElement('.top i');
    }

    public function testITADSectionAnimation(AcceptanceTester $I)
    {
        $I->wantToTest('that the ITAD section elements animate in');
        $I->scrollTo('footer');
        $I->wait(2);
        $I->seeNumberOfElements('.specialists h1', 1);
        $I->seeNumberOfElements('.specialists h2', 1);
        $I->seeNumberOfElements('.specialists h3', 8);
        $I->seeNumberOfElements('.specialists p', 8);
        $I->seeNumberOfElements('.specialists img', 8);
    }


    public function testGridSectionAnimation(AcceptanceTester $I)
    {
        $I->wantToTest('that the grid section elements animate in');
        $I->scrollTo('footer');
        $I->wait(2);
        $I->canSeeElement('.grid h1');
        $I->canSeeElement('.grid h2');
        $I->canSeeElement('.grid p');
        $I->canSeeElement('.grid i');
    }


    public function testBottomSectionAnimation(AcceptanceTester $I)
    {
        $I->wantToTest('that the bottom section elements animate in');
        $I->scrollTo('footer');
        $I->wait(3);
        $I->canSeeElement('.bottom h1');
        $I->seeNumberOfElements('.bottom li', 8);
    }

    public function testHeroForm(AcceptanceTester $I)
    {
        $I->wantToTest('that the hero form functions properly');
        $I->wait(2);
        $I->canSeeElement('.form-panel form');

        $I->executeJS('$("#category-dropdown").trigger("click");');
        $I->wait(2);
        $I->executeJS('$("#category-dropdown .dropdown-list .dropdown-list-item-1").trigger("click")');
        $I->wait(2);

        $I->executeJS('$("#type-dropdown").trigger("click");');
        $I->wait(2);
        $I->executeJS('$("#type-dropdown .dropdown-list .dropdown-list-item-1").trigger("click")');
        $I->wait(2);

        $I->fillField('.form-panel form input[name="address"]', 'test');
        $I->fillField('.form-panel form input[name="email"]', 'test@test.com');
        $I->fillField('.form-panel form input[name="phone"]', '2342342342');
        $I->click('#newsletter-no');
        $I->wait(1);
        $I->click('.form-panel form .btn');
        $I->wait(5);
        $I->canSeeElement('.form-panel .form-success');
    }

    public function testIncorrectForm(AcceptanceTester $I)
    {
        $I->wantToTest('that the an incorrect form submission functions properly');
        $I->wait(2);
        $I->canSeeElement('.form-panel form');
        $I->click('.form-panel form .btn');
        $I->wait(3);
        $I->seeNumberOfElements('.form-panel ul.validation-messages', 6);
    }

    public function testHeroFormMobile(AcceptanceTester $I)
    {
        $I->wantToTest('that the hero form functions properly on mobile');
        $I->reloadPage();
        $I->resizeWindow(414,736);
        $I->wait(2);

        $I->fillField('.mobile-quick-form .form-panel form input[name="address_mobile"]', 'test');
        $I->fillField('.mobile-quick-form .form-panel form input[name="email_mobile"]', 'test@test.com');
        $I->fillField('.mobile-quick-form .form-panel form input[name="phone_mobile"]', '2342342342');

        $I->executeJS('$("#category-dropdown_mobile").trigger("click");');
        $I->wait(2);
        $I->executeJS('$("#category-dropdown_mobile .dropdown-list .dropdown-list-item-1").trigger("click")');
        $I->wait(2);

        $I->executeJS('$("#type-dropdown_mobile").trigger("click");');
        $I->wait(2);
        $I->executeJS('$("#type-dropdown_mobile .dropdown-list .dropdown-list-item-1").trigger("click")');
        $I->wait(2);

        $I->click('#newsletter-no-mobile');
        $I->wait(1);


        $I->click('.mobile-quick-form .form-panel form .btn');
        $I->wait(3);
        $I->canSeeElement('.mobile-quick-form .form-panel .form-success');
        $I->resizeWindow(1920, 1200);


    }

    public function testIncorrectFormMobile(AcceptanceTester $I)
    {
        $I->wantToTest('that the an incorrect form submission functions properly');
        $I->resizeWindow(414,736);
        $I->wait(2);
        $I->wait(2);
        $I->canSeeElement('.mobile-quick-form .form-panel form');
        $I->click('.mobile-quick-form .form-panel form .btn');
        $I->wait(3);
        $I->seeNumberOfElements('.mobile-quick-form ul.validation-messages', 6);
        $I->resizeWindow(1920,1200);
    }




}
