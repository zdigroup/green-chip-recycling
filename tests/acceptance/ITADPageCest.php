<?php

require_once(__DIR__ . '/traits/CommonSiteTestsTrait.php');
require_once(__DIR__ . '/traits/HeroTestsTrait.php');

class ITADPageCest
{

    use CommonSiteTestsTrait;
    use HeroTestsTrait;

    public function _before(AcceptanceTester $I)
    {
        $I->amOnPage('/it-asset-disposition');
    }

    public function _after(AcceptanceTester $I)
    {
    }

    public function testPageLoad(AcceptanceTester $I)
    {
        $I->wantToTest('that the page content loads');
        $I->canSeeElement('.hero');
        $I->cantSeeElement('.application-error');
    }

    public function testTopSectionAnimation(AcceptanceTester $I)
    {
        $I->wantToTest('that the top section elements animate in');
        $I->scrollTo('footer');
        $I->wait(2);
        $I->seeNumberOfElements('.top h2', 2);
        $I->seeNumberOfElements('.top p', 2);
        $I->seeNumberOfElements('.top li', 7);
    }

    public function testChartsSectionAnimation(AcceptanceTester $I)
    {
        $I->wantToTest('that the charts section elements animate in');
        $I->scrollTo('footer');
        $I->wait(2);
        $I->seeNumberOfElements('.charts h1', 1);
        $I->seeNumberOfElements('.charts img', 1);
    }

    public function testAccordionsSectionAnimation(AcceptanceTester $I)
    {
        $I->wantToTest('that the accordions section elements animate in');
        $I->scrollTo('footer');
        $I->wait(2);
        $I->seeNumberOfElements('.accordions img', 6);
        $I->seeNumberOfElements('.accordions h3', 6);
        $I->seeNumberOfElements('.accordions p', 6);
        $I->seeNumberOfElements('.accordions a', 6);
    }

    public function testAccordionFunctionality(AcceptanceTester $I)
    {
        $I->wantToTest('that the accordions open and close properly');
        $I->scrollTo('.accordions');
        $I->wait(1);
        $I->cantSeeElement('.accordion');
        $I->click('#bucket-1 a');
        $I->wait(2);
        $I->canSeeElement('.accordion');
        $I->click('#bucket-1 a');
        $I->wait(2);
        $I->cantSeeElement('.accordion');
        $I->click('#bucket-2 a');
        $I->wait(2);
        $I->canSeeElement('.accordion');
        $I->click('#bucket-2 a');
        $I->wait(2);
        $I->cantSeeElement('.accordion');
    }

}
