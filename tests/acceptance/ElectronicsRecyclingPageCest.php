<?php

require_once(__DIR__ . '/traits/CommonSiteTestsTrait.php');
require_once(__DIR__ . '/traits/HeroTestsTrait.php');

class ElectronicsRecyclingPageCest
{

    use CommonSiteTestsTrait;
    use HeroTestsTrait;

    public function _before(AcceptanceTester $I)
    {
        $I->amOnPage('/electronics-recycling');
    }

    public function _after(AcceptanceTester $I)
    {
    }

    public function testPageLoad(AcceptanceTester $I)
    {
        $I->wantToTest('that the page content loads');
        $I->canSeeElement('.hero');
        $I->cantSeeElement('.application-error');
    }


    public function testMainSectionAnimation(AcceptanceTester $I)
    {
        $I->wantToTest('that the main section elements animate in');
        $I->scrollTo('footer');
        $I->wait(2);
        $I->seeNumberOfElements('.main h2', 1);
        $I->seeNumberOfElements('.main p', 3);
        $I->seeNumberOfElements('.main li', 5);
    }

}