<?php

require_once(__DIR__ . '/traits/CommonSiteTestsTrait.php');
require_once(__DIR__ . '/traits/HeroTestsTrait.php');

class ContactPageCest
{

    use CommonSiteTestsTrait;
    use HeroTestsTrait;

    public function _before(AcceptanceTester $I)
    {
        $I->amOnPage('/contact');
    }

    public function _after(AcceptanceTester $I)
    {
    }

    public function testPageLoad(AcceptanceTester $I)
    {
        $I->wantToTest('that the page content loads');
        $I->canSeeElement('.hero');
        $I->cantSeeElement('.application-error');
    }


    public function testContactForm(AcceptanceTester $I)
    {
        $I->wantToTest('that the contact form functions properly');
        $I->scrollTo('.form form');
        $I->wait(2);
        $I->canSeeElement('.form form');

        $I->executeJS('$("#category-dropdown").trigger("click");');
        $I->wait(2);
        $I->executeJS('$("#category-dropdown .dropdown-list .dropdown-list-item-1").trigger("click")');
        $I->wait(2);

        $I->executeJS('$("#type-dropdown").trigger("click");');
        $I->wait(2);
        $I->executeJS('$("#type-dropdown .dropdown-list .dropdown-list-item-1").trigger("click")');
        $I->wait(2);

        $I->fillField('.form form[name="form.form"] input[name="name"]', 'test');
        $I->fillField('.form form[name="form.form"] input[name="email"]', 'test@test.com');
        $I->fillField('.form form[name="form.form"] input[name="address"]', 'test');
        $I->fillField('.form form[name="form.form"] textarea[name="comments"]', 'test');
        $I->click('#newsletter-no');

        $I->click('.form form .btn');
        $I->wait(3);
        $I->canSeeElement('.form-success');
    }

    public function testContactFormMobile(AcceptanceTester $I)
    {
        $I->wantToTest('that the contact form functions on mobile');
        $I->resizeWindow(414,736);
        $I->wait(2);
        $this->testContactForm($I);
        $I->resizeWindow(1920,1200);
    }

    public function testIncorrectForm(AcceptanceTester $I)
    {
        $I->wantToTest('that the an incorrect form submission functions properly');
        $I->canSeeElement('.form form');
        $I->click('.form form .btn');
        $I->wait(3);
        $I->seeNumberOfElements('ul.validation-messages', 6);
    }

}
