<?php

require_once(__DIR__ . '/traits/CommonSiteTestsTrait.php');


class AccessibilityPageCest
{

    use CommonSiteTestsTrait;


    public function _before(AcceptanceTester $I)
    {
        $I->amOnPage('/accessibility');
    }

    public function _after(AcceptanceTester $I)
    {
    }

    public function testPageLoad(AcceptanceTester $I)
    {
        $I->wantToTest('that the page content loads');
        $I->canSeeElement('.content');
        $I->cantSeeElement('.application-error');
    }


}
