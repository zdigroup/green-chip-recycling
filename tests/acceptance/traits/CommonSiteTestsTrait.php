<?php

trait CommonSiteTestsTrait
{

    public function testNavigationMenu(AcceptanceTester $I)
    {
        $I->wantToTest('that the menu functions properly');
        $I->cantSee('.menu');
        $I->click('.menu-toggle');
        $I->wait(2);
        $I->canSee('Home',  '.menu ul li');
        $I->click('.menu-toggle');
        $I->wait(2);
        $I->cantSee('Home', '.menu ul li');
    }


    public function testCTABar(AcceptanceTester $I)
    {
        $I->wantToTest('that the cta bar functions properly');
        $I->seeElement('.fixed-bar');
        $I->executeJS('scroll(0,0);');
        $I->wait(2);
        $checkAnchored = $I->executeJS('return $(".fixed-bar").hasClass("anchored"); ');

        if($checkAnchored)
        {
            throw new \Codeception\Exception\Fail('Fixed bar should not be anchored on page load');
        }

        $I->scrollTo('footer');
        $I->wait(2);

        $checkAnchored = $I->executeJS('return $(".fixed-bar").hasClass("anchored"); ');

        if(!$checkAnchored)
        {
            throw new \Codeception\Exception\Fail('Fixed bar should be anchored');
        }

        $I->executeJS('scroll(0,0);');
        $I->wait(2);

        $checkAnchored = $I->executeJS('return $(".fixed-bar").hasClass("anchored"); ');

        if($checkAnchored)
        {
            throw new \Codeception\Exception\Fail('Fixed bar should not be anchored');
        }

    }


}