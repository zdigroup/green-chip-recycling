<?php

trait HeroTestsTrait
{

    public function testHeroAnimation(AcceptanceTester $I)
    {
        $I->wantToTest('that the hero elements animate in properly');
        $I->wait(2);
        $I->canSeeElement('.hero h2');
        $I->canSeeElement('.hero h1');
    }

}