<?php

require_once(__DIR__ . '/traits/CommonSiteTestsTrait.php');


class EventsPageCest
{

    use CommonSiteTestsTrait;


    public function _before(AcceptanceTester $I)
    {
        $I->amOnPage('/events');
    }

    public function _after(AcceptanceTester $I)
    {
    }

    public function testPageLoad(AcceptanceTester $I)
    {
        $I->wantToTest('that the page content loads');
        $I->canSeeElement('.hero');
        $I->cantSeeElement('.application-error');
    }

    public function testCTABar(AcceptanceTester $I)
    {
        // override, page isn't long enough to test
    }


}
