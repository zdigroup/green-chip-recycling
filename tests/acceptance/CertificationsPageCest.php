<?php

require_once(__DIR__ . '/traits/CommonSiteTestsTrait.php');
require_once(__DIR__ . '/traits/HeroTestsTrait.php');

class CertificationsPageCest
{

    use CommonSiteTestsTrait;
    use HeroTestsTrait;

    public function _before(AcceptanceTester $I)
    {
        $I->amOnPage('/certifications');
    }

    public function _after(AcceptanceTester $I)
    {
    }

    public function testPageLoad(AcceptanceTester $I)
    {
        $I->wantToTest('that the page content loads');
        $I->canSeeElement('.hero');
        $I->cantSeeElement('.application-error');
    }

    public function testTopSectionAnimation(AcceptanceTester $I)
    {
        $I->wantToTest('that the top section elements animate in');
        $I->scrollTo('footer');
        $I->wait(2);
        $I->seeNumberOfElements('.top h2', 1);
        $I->seeNumberOfElements('.top p', 1);
    }

    public function testBottomSectionAnimation(AcceptanceTester $I)
    {
        $I->wantToTest('that the bottom section elements animate in');
        $I->scrollTo('footer');
        $I->wait(2);
        $I->seeNumberOfElements('.bottom img', 5);
        $I->seeNumberOfElements('.bottom h2', 5);
        $I->seeNumberOfElements('.bottom p', 6);
        $I->seeNumberOfElements('.bottom li', 5);
    }

}