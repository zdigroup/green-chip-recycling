<?php

require_once(__DIR__ . '/traits/CommonSiteTestsTrait.php');
require_once(__DIR__ . '/traits/HeroTestsTrait.php');

class OEMPageCest
{

    use CommonSiteTestsTrait;
    use HeroTestsTrait;

    public function _before(AcceptanceTester $I)
    {
        $I->amOnPage('/oem-program-management');
    }

    public function _after(AcceptanceTester $I)
    {
    }

    public function testPageLoad(AcceptanceTester $I)
    {
        $I->wantToTest('that the page content loads');
        $I->canSeeElement('.hero');
        $I->cantSeeElement('.application-error');
    }

    public function testTopSectionAnimation(AcceptanceTester $I)
    {
        $I->wantToTest('that the top section elements animate in');
        $I->scrollTo('footer');
        $I->wait(2);
        $I->canSeeElement('.top h2');
        $I->canSeeElement('.top img');
        $I->seeNumberOfElements('.top p', 2);
    }





}
