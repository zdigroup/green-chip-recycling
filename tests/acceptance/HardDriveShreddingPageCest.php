<?php

require_once(__DIR__ . '/traits/CommonSiteTestsTrait.php');
require_once(__DIR__ . '/traits/HeroTestsTrait.php');

class HardDriveShreddingPageCest
{

    use CommonSiteTestsTrait;
    use HeroTestsTrait;

    public function _before(AcceptanceTester $I)
    {
        $I->amOnPage('/hard-drive-shredding');
    }

    public function _after(AcceptanceTester $I)
    {
    }

    public function testPageLoad(AcceptanceTester $I)
    {
        $I->wantToTest('that the page content loads');
        $I->canSeeElement('.hero');
        $I->cantSeeElement('.application-error');
    }

    public function testTopSectionAnimation(AcceptanceTester $I)
    {
        $I->wantToTest('that the top section elements animate in');
        $I->scrollTo('footer');
        $I->wait(2);
        $I->seeNumberOfElements('.top h2', 1);
        $I->seeNumberOfElements('.top p', 2);
        $I->seeNumberOfElements('.top img', 1);
    }

    public function testOffersSectionAnimation(AcceptanceTester $I)
    {
        $I->wantToTest('that the offers section elements animate in');
        $I->scrollTo('footer');
        $I->wait(2);
        $I->seeNumberOfElements('.offers h2', 1);
        $I->seeNumberOfElements('.offers li', 5);
    }

    public function testGridSectionAnimation(AcceptanceTester $I)
    {
        $I->wantToTest('that the bottom section elements animate in');
        $I->scrollTo('footer');
        $I->wait(2);
        $I->seeNumberOfElements('.grid h2', 1);
        $I->seeNumberOfElements('.grid p', 2);
    }

}