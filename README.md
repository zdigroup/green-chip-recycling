# Green Chip Application #


## How do I get set up? ##

* Clone this repository ```git clone git@bitbucket.org:spectravp/green-chip-recycling.git```
* Checkout the ```master``` branch
* Run: ```composer install```
* Run: ```bower update```
* Run: ```npm install```
* Run: ```grunt development-fast```
* Run: ```docker-compose up```

Navigate to ```localhost``` for the local environment once docker is running.

### Who do I talk to?

* [Derek Miranda at ZDI](mailto:derek@zdidesign.com)

