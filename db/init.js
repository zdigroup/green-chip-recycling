var db = new Mongo().getDB("green_chip");
db.createUser({"user":"green_chip","pwd":"ZunVUA8UG[Y[)8-<","roles":["dbAdmin"]});
db.getCollection("administrators").insert({
    "_id" : ObjectId("5679b5a137c3b8396372190c"),
    "active" : true,
    "email" : "derek@zdidesign.com",
    "first_name" : "Derek",
    "last_name" : "Miranda",
    "password" : "$2y$10$l7796BI23rGqE3NzW7h9SO9tVi7epZkbjCFzWi5Mu0eP3gI0QyXLO"
});

// PWD: tester1234
db.getCollection("administrators").insert({
        "_id" : ObjectId("5941c99b8930783f510108a7"),
        "email" : "test@test.com",
        "active" : true,
        "first_name" : "Test",
        "last_name" : "User",
        "password" : "$2y$10$gz/Feo8/z8qm6LO5l3kqM.ecRMI91DFGdDFFMoSFyM7y02BbmBzRG",
    }
)

db.getCollection('oauth_clients').insert(
    {
        "_id" : ObjectId("5679b4f537c3b83963721907"),
        "client_id" : "applicationAdmin",
        "grant_types" : "client_credentials password refresh_token",
        "scope" : "admin",
        "client_secret" : "$2y$10$N8PcR1QIP90v5MI8o/64iemQdYyfP9EwsCa76DXjJy5p.OqzSXeeO"
    }
);

db.getCollection('oauth_clients').insert(
    {
        "_id" : ObjectId("5679b57d37c3b8396372190b"),
        "client_id" : "application",
        "grant_types" : "client_credentials password refresh_token",
        "scope" : "api",
        "client_secret" : "$2y$10$N8PcR1QIP90v5MI8o/64iemQdYyfP9EwsCa76DXjJy5p.OqzSXeeO"
    }
);

db.getCollection('oauth_scopes').insert(
    {
        "_id" : ObjectId("5679b51337c3b83963721908"),
        "type" : "supported",
        "scope" : "admin",
        "client_id" : "applicationAdmin"
    }
);

db.getCollection('oauth_scopes').insert(
    {
        "_id" : ObjectId("5679b56537c3b8396372190a"),
        "type" : "supported",
        "scope" : "api",
        "client_id" : "application"
    }
);

db.getCollection('oauth_users').insert(
    {
        "_id" : ObjectId("5679b53337c3b83963721909"),
        "first_name" : "Derek",
        "last_name" : "Miranda",
        "password" : "$2y$10$5EIh9KuQI8qhi/0EqAjb3uZkuVOyJpP3JoUIKrd29V6UreeGNimki",
        "username" : "derek@zdidesign.com"
    }
);

// Inject permanent access token
db.getCollection('oauth_access_tokens').insert(
    {
        "_id" : ObjectId("5942bf50893078369e3fca32"),
        "access_token" : "b13f1595132d3b95f7624a34ca7136192cec2d44",
        "client_id" : "applicationAdmin",
        "expires" : 2541517776,
        "user_id" : null,
        "scope" : "admin"
    }
);

var db = new Mongo().getDB("green_chip_testing");
db.createUser({"user":"green_chip","pwd":"ZunVUA8UG[Y[)8-<","roles":["dbAdmin"]});
db.getCollection("administrators").insert({
    "_id" : ObjectId("5679b5a137c3b8396372190c"),
    "active" : true,
    "email" : "derek@zdidesign.com",
    "first_name" : "Derek",
    "last_name" : "Miranda",
    "password" : "$2y$10$l7796BI23rGqE3NzW7h9SO9tVi7epZkbjCFzWi5Mu0eP3gI0QyXLO"
});

// PWD: tester1234
db.getCollection("administrators").insert({
        "_id" : ObjectId("5941c99b8930783f510108a7"),
        "email" : "test@test.com",
        "active" : true,
        "first_name" : "Test",
        "last_name" : "User",
        "password" : "$2y$10$gz/Feo8/z8qm6LO5l3kqM.ecRMI91DFGdDFFMoSFyM7y02BbmBzRG",
    }
)

db.getCollection('oauth_clients').insert(
    {
        "_id" : ObjectId("5679b4f537c3b83963721907"),
        "client_id" : "applicationAdmin",
        "grant_types" : "client_credentials password refresh_token",
        "scope" : "admin",
        "client_secret" : "$2y$10$N8PcR1QIP90v5MI8o/64iemQdYyfP9EwsCa76DXjJy5p.OqzSXeeO"
    }
);

db.getCollection('oauth_clients').insert(
    {
        "_id" : ObjectId("5679b57d37c3b8396372190b"),
        "client_id" : "application",
        "grant_types" : "client_credentials password refresh_token",
        "scope" : "api",
        "client_secret" : "$2y$10$N8PcR1QIP90v5MI8o/64iemQdYyfP9EwsCa76DXjJy5p.OqzSXeeO"
    }
);

db.getCollection('oauth_scopes').insert(
    {
        "_id" : ObjectId("5679b51337c3b83963721908"),
        "type" : "supported",
        "scope" : "admin",
        "client_id" : "applicationAdmin"
    }
);

db.getCollection('oauth_scopes').insert(
    {
        "_id" : ObjectId("5679b56537c3b8396372190a"),
        "type" : "supported",
        "scope" : "api",
        "client_id" : "application"
    }
);

db.getCollection('oauth_users').insert(
    {
        "_id" : ObjectId("5679b53337c3b83963721909"),
        "first_name" : "Derek",
        "last_name" : "Miranda",
        "password" : "$2y$10$5EIh9KuQI8qhi/0EqAjb3uZkuVOyJpP3JoUIKrd29V6UreeGNimki",
        "username" : "derek@zdidesign.com"
    }
);

// Inject permanent access token
db.getCollection('oauth_access_tokens').insert(
    {
        "_id" : ObjectId("5942bf50893078369e3fca32"),
        "access_token" : "b13f1595132d3b95f7624a34ca7136192cec2d44",
        "client_id" : "applicationAdmin",
        "expires" : 2541517776,
        "user_id" : null,
        "scope" : "admin"
    }
);



