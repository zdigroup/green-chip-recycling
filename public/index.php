<?php
/**
 * @license   http://opensource.org/licenses/BSD-3-Clause BSD-3-Clause
 * @copyright Copyright (c) 2014-2016 Zend Technologies USA Inc. (http://www.zend.com)
 */

use Zend\Stdlib\ArrayUtils;
use ZF\Apigility\Application;

ini_set('display_errors',0);
ini_set('error_reporting', -1);

$environment = isset($_SERVER['ApplicationEnvironment']) ? $_SERVER['ApplicationEnvironment'] : 'development';

switch($environment)
{
    case 'production':
    {
        define('GLOB_PATH', '{global,local}.php');
        break;
    }
    case 'testing':
    {
        define('GLOB_PATH', '{global,local,testing}.php');
        break;
    }
    default:
    {
        define('GLOB_PATH', '{global,local,testing,development}.php');
    }
}

$protocol = 'http://';

if( $_SERVER['SERVER_PORT'] == '443')
{
    $protocol = 'https://';
}


define('APPLICATION_BASE_URL', $protocol . $_SERVER['HTTP_HOST']);

/**
 * This makes our life easier when dealing with paths. Everything is relative
 * to the application root now.
 */
chdir(dirname(__DIR__));

// Redirect legacy requests to enable/disable development mode to new tool
if (php_sapi_name() === 'cli'
    && $argc > 2
    && 'development' == $argv[1]
    && in_array($argv[2], ['disable', 'enable'])
) {
    // Windows needs to execute the batch scripts that Composer generates,
    // and not the Unix shell version.
    $script = defined('PHP_WINDOWS_VERSION_BUILD') && constant('PHP_WINDOWS_VERSION_BUILD')
        ? '.\\vendor\\bin\\zf-development-mode.bat'
        : './vendor/bin/zf-development-mode';
    system(sprintf('%s %s', $script, $argv[2]), $return);
    exit($return);
}

// Decline static file requests back to the PHP built-in webserver
if (php_sapi_name() === 'cli-server' && is_file(__DIR__ . parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH))) {
    return false;
}

if (! file_exists('vendor/autoload.php')) {
    throw new RuntimeException(
        'Unable to load application.' . PHP_EOL
        . '- Type `composer install` if you are developing locally.' . PHP_EOL
        . '- Type `vagrant ssh -c \'composer install\'` if you are using Vagrant.' . PHP_EOL
        . '- Type `docker-compose run apigility composer install` if you are using Docker.'
    );
}

// Setup autoloading
include 'vendor/autoload.php';

$appConfig = include 'config/application.config.php';

if (file_exists('config/development.config.php')) {
    $appConfig = ArrayUtils::merge(
        $appConfig,
        include 'config/development.config.php'
    );
}

// Run the application!
try {
    Application::init($appConfig)->run();
} catch(Exception $e)
{
    echo $e->getMessage();
    echo $e->getTraceAsString();
    http_response_code(500);
    echo json_encode(array(
        'title'=>$e->getMessage(),
        'file'=>$e->getFile(),
        'line'=>$e->getLine(),
        'trace'=>$e->getTrace(),
    ));
    exit;
}